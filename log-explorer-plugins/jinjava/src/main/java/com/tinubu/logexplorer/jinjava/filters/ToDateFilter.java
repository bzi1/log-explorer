/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.jinjava.filters;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;

import com.hubspot.jinjava.interpret.JinjavaInterpreter;
import com.hubspot.jinjava.lib.filter.Filter;
import com.tinubu.logexplorer.core.config.Configuration.Parameters;
import com.tinubu.logexplorer.jinjava.JinjavaFormatter;

/**
 * Converts a string to ZonedDateTime.
 * <p>
 * If string represents a local date time, use the {@value PARSE_TIMEZONE_PARAMETER} parameter
 * to complete missing timezone or {@link #DEFAULT_PARSE_TIMEZONE} timezone.
 * <p>
 * Resulting zoned date time is then displayed in globally configured timezone.
 * <p>
 * Syntax : toDate(format)
 * <p>
 * See {@link DateTimeFormatter}
 */
public class ToDateFilter implements Filter {

   private static final String PARSE_TIMEZONE_PARAMETER = "jinjava.filter.todate.parse-timezone";
   /** Default timezone only used to complete local date time from parsing. */
   private static final ZoneId DEFAULT_PARSE_TIMEZONE = ZoneId.systemDefault();

   @Override
   public Object filter(Object var, JinjavaInterpreter interpreter, String... args) {
      if (args.length == 0) {
         throw new IllegalArgumentException(String.format(
               "'%s' filter must have at least one date format attribute",
               getName()));
      }

      TemporalAccessor date = DateTimeFormatter
            .ofPattern(args[0])
            .parseBest(String.valueOf(var), ZonedDateTime::from, LocalDateTime::from);

      Parameters parameters =
            (Parameters) interpreter.getContext().get(JinjavaFormatter.PARAMETERS_CONTEXT_KEY);
      ZoneId parsingTimezone =
            parameters.stringValue(PARSE_TIMEZONE_PARAMETER).map(ZoneId::of).orElse(DEFAULT_PARSE_TIMEZONE);
      ZoneId displayTimezone = (ZoneId) interpreter.getContext().get(JinjavaFormatter.TIMEZONE_CONTEXT_KEY);

      ZonedDateTime zdt;
      if (date instanceof ZonedDateTime) {
         ZonedDateTime retrievedDate = (ZonedDateTime) date;
         zdt = retrievedDate.withZoneSameInstant(displayTimezone);
      } else if (date instanceof LocalDateTime) {
         zdt = ((LocalDateTime) date).atZone(parsingTimezone);
      } else {
         throw new IllegalStateException(String.format("Unknown date type '%s'", date));
      }

      return zdt;
   }

   @Override
   public String getName() {
      return "toDate";
   }
}
