/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.kubernetes;

import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tinubu.commons.lang.util.Pair;
import com.tinubu.logexplorer.core.backend.Backend;
import com.tinubu.logexplorer.core.backend.BackendConfiguration;
import com.tinubu.logexplorer.core.backend.BackendInterruptionState;
import com.tinubu.logexplorer.core.config.Configuration.Parameters;
import com.tinubu.logexplorer.core.extension.parameter.ParameterDescription;
import com.tinubu.logexplorer.core.extension.parameter.completers.BooleanCompleter;
import com.tinubu.logexplorer.core.extension.parameter.completers.CacheCompleter;
import com.tinubu.logexplorer.core.search.QueryDateRange;
import com.tinubu.logexplorer.core.search.SearchResult;
import com.tinubu.logexplorer.core.search.SearchResult.Attributes;
import com.tinubu.logexplorer.core.search.query.QuerySpecification;

import io.kubernetes.client.openapi.ApiClient;
import io.kubernetes.client.openapi.ApiException;
import io.kubernetes.client.openapi.apis.CoreV1Api;
import io.kubernetes.client.openapi.models.V1Container;
import io.kubernetes.client.openapi.models.V1Namespace;
import io.kubernetes.client.openapi.models.V1NamespaceList;
import io.kubernetes.client.openapi.models.V1Pod;
import io.kubernetes.client.openapi.models.V1PodList;
import io.kubernetes.client.util.ClientBuilder;
import io.kubernetes.client.util.credentials.AccessTokenAuthentication;
import io.kubernetes.client.util.credentials.UsernamePasswordAuthentication;
import okhttp3.Call;
import okhttp3.Response;

public class KubernetesBackend implements Backend {

   private static final Logger logger = LoggerFactory.getLogger(KubernetesBackend.class);

   /** Verify SSL parameter definition. */
   private static final Pair<String, Boolean> VERIFY_SSL_PARAMETER =
         Pair.of("kubernetes.backend.verify-ssl", true);
   /** Page size parameter definition. */
   private static final Pair<String, Integer> PAGE_SIZE_PARAMETER =
         Pair.of("kubernetes.backend.page-size", 1000);
   /** Log group parameter definition. */
   private static final Pair<String, String> NAMESPACE_PARAMETER =
         Pair.of("kubernetes.backend.namespace", null);
   /** Stream prefix parameter definition. */
   private static final Pair<String, String> POD_PREFIX_PARAMETER =
         Pair.of("kubernetes.backend.pod-prefix", null);
   /** Label selector parameter definition. */
   private static final Pair<String, String> POD_LABEL_SELECTOR_PARAMETER =
         Pair.of("kubernetes.backend.pod-label-selector", null);
   /** Field selector parameter definition. */
   private static final Pair<String, String> POD_FIELD_SELECTOR_PARAMETER =
         Pair.of("kubernetes.backend.pod-field-selector", null);
   /** Container parameter definition. */
   private static final Pair<String, String> CONTAINER_PARAMETER =
         Pair.of("kubernetes.backend.container", null);
   /**
    * Feature flag to enable retrieving available namespaces from API, but most of the time, access is not
    * granted.
    */
   private static final boolean CAN_API_LIST_ALL_NAMESPACES = true;
   /** Fixed read timeout in ms used in follow mode. */
   private static final int FOLLOW_MODE_READ_TIMEOUT = 4000 * 1000; // FIXME what's the use ?
   private static final int NAMESPACES_CACHE_DURATION = 120;
   private static final int PODS_CACHE_DURATION = 20;
   private static final int PODS_CONTAINERS_CACHE_DURATION = 20;

   private final Parameters parameters;
   private final CoreV1Api api;

   public KubernetesBackend(Parameters parameters, BackendConfiguration configuration) {
      this.parameters = notNull(parameters, "parameters");
      notNull(configuration, "configuration");

      configureJavaForGoServer();

      try {
         ClientBuilder clientBuilder = ClientBuilder.standard(false).setVerifyingSsl(verifySsl());

         if (configuration.backendUris() != null && !configuration.backendUris().isEmpty()) {
            clientBuilder.setBasePath(configuration.backendUris().get(0).toString());
         }

         if (configuration.authenticationToken() != null) {
            clientBuilder.setAuthentication(new AccessTokenAuthentication(configuration.authenticationToken()));
         } else if (configuration.authenticationUser() != null
                    && configuration.authenticationPassword() != null) {
            clientBuilder.setAuthentication(new UsernamePasswordAuthentication(configuration.authenticationUser(),
                                                                               configuration.authenticationPassword()));
         }

         ApiClient client = clientBuilder.build();

         client.setConnectTimeout(configuration.connectTimeout());
         client.setReadTimeout(configuration.socketTimeout());

         api = new CoreV1Api(client);
      } catch (IOException e) {
         throw new IllegalStateException(String.format("Error using '%s'", configuration.backendUris()), e);
      }
   }

   @Override
   // FIXME CTRL-C is not responsive in follow mode while there's no activity, because InputStreamReader is blocking.
   // FIXME Search result should not contain only one line at action call. Can be implemented if we have a
   //       non-blocking stream reader (see FIXME above).
   public void search(BiConsumer<SearchResult, QuerySpecification> action,
                      QuerySpecification querySpecification,
                      QueryDateRange dateRange,
                      Long numberLines,
                      boolean followMode) {
      Optional<V1Pod> contextPod = contextPod();

      if (contextPod.isEmpty()) {
         throw new IllegalStateException(String.format(
               "Not unique pod matching the current selection parameters : %s",
               contextPods(true).stream().map(pod -> pod.getMetadata().getName()).collect(joining(","))));
      }

      int sinceSeconds = Long
            .valueOf(ChronoUnit.SECONDS.between(dateRange.fromDate(),
                                                ZonedDateTime.now(dateRange.fromDate().getZone())))
            .intValue();

      apiWithReadTimeout(api, api -> {
         try {
            Call call = api.readNamespacedPodLogCall(contextPod.get().getMetadata().getName(),
                                                     contextPod.get().getMetadata().getNamespace(),
                                                     container().orElse(null),
                                                     followMode,
                                                     false,
                                                     null,
                                                     "false",
                                                     false,
                                                     sinceSeconds,
                                                     numberLines != null ? numberLines.intValue() : null,
                                                     true,
                                                     null);
            Response response = call.execute();
            if (!response.isSuccessful()) {
               throw new IllegalStateException("Logs request failed : " + response.body().string());
            }
            try (BufferedReader logStream = new BufferedReader(new InputStreamReader(response
                                                                                           .body()
                                                                                           .byteStream()))) {
               String logLine;
               while (!BackendInterruptionState.interrupting() && (logLine = logStream.readLine()) != null) {
                  action.accept(searchResult(contextPod.get(), logLine), querySpecification);
               }
            }
         } catch (ApiException e) {
            throw new IllegalStateException(e.getResponseBody(), e);
         } catch (IOException e) {
            throw new IllegalStateException(e);
         }
      }, followMode ? FOLLOW_MODE_READ_TIMEOUT : api.getApiClient().getReadTimeout());
   }

   /**
    * @implSpec Evict idle connections in okHttp pool or exiting application will block for some time.
    */
   @Override
   public void close() {
      api.getApiClient().getHttpClient().connectionPool().evictAll();
   }

   @Override
   public List<ParameterDescription<?>> parameters() {
      return Arrays.asList(new ParameterDescription<>(PAGE_SIZE_PARAMETER.getKey(),
                                                      Integer.class,
                                                      PAGE_SIZE_PARAMETER.getValue(),
                                                      "request page size"),
                           new ParameterDescription<>(NAMESPACE_PARAMETER.getKey(),
                                                      String.class,
                                                      NAMESPACE_PARAMETER.getValue(),
                                                      "optional namespace name (all namespaces by default)").completer(
                                 new CacheCompleter(__ -> allNamespaces()
                                       .stream()
                                       .map(ns -> ns.getMetadata().getName())
                                       .collect(toList()), NAMESPACES_CACHE_DURATION)),
                           new ParameterDescription<>(POD_PREFIX_PARAMETER.getKey(),
                                                      String.class,
                                                      POD_PREFIX_PARAMETER.getValue(),
                                                      "optional pod name prefix").completer(new CacheCompleter(
                                 __ -> contextPods(false)
                                       .stream()
                                       .map(pod -> pod.getMetadata().getName())
                                       .collect(toList()),
                                 PODS_CACHE_DURATION)),
                           new ParameterDescription<>(CONTAINER_PARAMETER.getKey(),
                                                      String.class,
                                                      CONTAINER_PARAMETER.getValue(),
                                                      "optional pod container name (all pod containers by default)").completer(
                                 new CacheCompleter(__ -> contextPodContainers()
                                       .stream()
                                       .map(V1Container::getName)
                                       .collect(toList()), PODS_CONTAINERS_CACHE_DURATION)),
                           new ParameterDescription<>(POD_LABEL_SELECTOR_PARAMETER.getKey(),
                                                      String.class,
                                                      POD_LABEL_SELECTOR_PARAMETER.getValue(),
                                                      "optional pod label selector"),
                           new ParameterDescription<>(POD_FIELD_SELECTOR_PARAMETER.getKey(),
                                                      String.class,
                                                      POD_FIELD_SELECTOR_PARAMETER.getValue(),
                                                      "optional pod field selector"),
                           new ParameterDescription<>(VERIFY_SSL_PARAMETER.getKey(),
                                                      Boolean.class,
                                                      VERIFY_SSL_PARAMETER.getValue(),
                                                      "verify SSL").completer(new BooleanCompleter()));
   }

   /**
    * Kubernetes/Go server is not compatible with TLSv1.3's OpenJDK implementation.
    * <p>
    * See https://github.com/golang/go/issues/35722#issuecomment-571173416
    */
   private void configureJavaForGoServer() {
      System.setProperty("jdk.tls.client.protocols", "TLSv1.2");
   }

   /**
    * Temporarily overrides an API client read timeout.
    *
    * @param api api to update
    * @param apiConsumer an operation to execute with updated api client
    * @param readTimeout new read timeout
    */
   private void apiWithReadTimeout(CoreV1Api api, Consumer<CoreV1Api> apiConsumer, int readTimeout) {
      int saveReadTimeout = api.getApiClient().getReadTimeout();
      api.getApiClient().setReadTimeout(readTimeout);
      try {
         apiConsumer.accept(api);
      } finally {
         api.getApiClient().setReadTimeout(saveReadTimeout);
      }
   }

   /**
    * Returns contextual pod or empty list if there's not one and only one contextual pod defined.
    */
   private Optional<V1Pod> contextPod() {
      List<V1Pod> contextPods = contextPods(true);
      if (contextPods.size() != 1) {
         return Optional.empty();
      } else {
         return Optional.of(contextPods.get(0));
      }
   }

   /**
    * Returns a list of pod containers for the current context.
    * Returns an empty list if contextual pod is not defined.
    */
   private List<V1Container> contextPodContainers() {
      return contextPod()
            .map(contextPod -> namespacePod(contextPod.getMetadata().getNamespace(),
                                            contextPod.getMetadata().getName()).getSpec().getContainers())
            .orElse(emptyList());
   }

   private List<V1Pod> contextPods(boolean filterPodPrefix) {
      List<V1Pod> contextPods;
      String namespace = namespace().orElse(null);
      if (namespace == null) {
         contextPods = allNamespacesPods();
      } else {
         contextPods = namespacePods(namespace().get());
      }

      if (filterPodPrefix) {
         Optional<String> podNamePrefix = podPrefix();
         return contextPods
               .stream()
               .filter(pod -> podNamePrefix
                     .map(prefix -> pod.getMetadata().getName().startsWith(prefix))
                     .orElse(true))
               .collect(toList());
      } else {
         return contextPods;
      }
   }

   private List<V1Namespace> allNamespaces() {
      List<V1Namespace> namespaces = new ArrayList<>();

      if (CAN_API_LIST_ALL_NAMESPACES) {
         String _continue = null;
         do {

            try {
               V1NamespaceList response =
                     api.listNamespace(null, null, null, null, null, pageSize(), null, null, null, false);
               namespaces.addAll(response.getItems());
               if (response.getMetadata().getContinue() != null) {
                  _continue = response.getMetadata().getContinue();
               }

            } catch (ApiException e) {
               if (e.getCode() == 403) {
                  logger.debug("Error while retrieving namespaces", e);
                  break;
               } else {
                  throw new IllegalStateException(e.getResponseBody(), e);
               }
            }
         } while (_continue != null);
      }

      return namespaces;
   }

   private List<V1Pod> namespacePods(String namespace) {
      List<V1Pod> pods = new ArrayList<>();

      String _continue = null;
      do {
         try {
            V1PodList response = api.listNamespacedPod(namespace,
                                                       null,
                                                       null,
                                                       _continue,
                                                       podFieldSelector().orElse(null),
                                                       podLabelSelector().orElse(null),
                                                       pageSize(),
                                                       null,
                                                       null,
                                                       null,
                                                       false);

            pods.addAll(response.getItems());
            if (response.getMetadata().getContinue() != null) {
               _continue = response.getMetadata().getContinue();
            }
         } catch (ApiException e) {
            if (e.getCode() == 403) {
               logger.debug("Error while retrieving '{}' namespace pods", namespace, e);
               break;
            } else {
               throw new IllegalStateException(e.getResponseBody(), e);
            }
         }
      } while (_continue != null);

      return pods;
   }

   private List<V1Pod> allNamespacesPods() {
      List<V1Pod> pods = new ArrayList<>();

      String _continue = null;
      do {
         try {
            V1PodList response = api.listPodForAllNamespaces(null,
                                                             _continue,
                                                             podFieldSelector().orElse(null),
                                                             podLabelSelector().orElse(null),
                                                             pageSize(),
                                                             null,
                                                             null,
                                                             null,
                                                             null,
                                                             false);
            pods.addAll(response.getItems());
            if (response.getMetadata().getContinue() != null) {
               _continue = response.getMetadata().getContinue();
            }
         } catch (ApiException e) {
            if (e.getCode() == 403) {
               logger.debug("Error while retrieving all namespaces pods", e);
               break;
            } else {
               throw new IllegalStateException(e.getResponseBody(), e);
            }
         }

      } while (_continue != null);

      return pods;
   }

   private V1Pod namespacePod(String namespace, String pod) {
      try {
         return api.readNamespacedPod(pod, namespace, null, null, null);
      } catch (ApiException e) {
         throw new IllegalStateException(e.getResponseBody(), e);
      }
   }

   private SearchResult searchResult(V1Pod pod, String logLine) {
      List<String> lineComponents = Arrays.asList(logLine.split(" "));

      Attributes kubernetes = new Attributes()
            .withAttribute("namespace_name", pod.getMetadata().getNamespace())
            .withAttribute("pod_name", pod.getMetadata().getName())
            .withAttribute("cluster_name", pod.getMetadata().getClusterName());

      Attributes attributes = new Attributes()
            .withAttribute("timestamp",
                           ZonedDateTime.parse(lineComponents.get(0), DateTimeFormatter.ISO_DATE_TIME))
            .withAttribute("message", lineComponents.stream().skip(1).collect(joining(" ")))
            .withAttribute("kubernetes", kubernetes);

      return new SearchResult(singletonList(attributes));
   }

   private int pageSize() {
      return parameters.intValue(PAGE_SIZE_PARAMETER.getKey()).orElse(PAGE_SIZE_PARAMETER.getValue());
   }

   private Optional<String> namespace() {
      return parameters.stringValue(NAMESPACE_PARAMETER.getKey());
   }

   private Optional<String> podPrefix() {
      return parameters.stringValue(POD_PREFIX_PARAMETER.getKey());
   }

   private Optional<String> podLabelSelector() {
      return parameters.stringValue(POD_LABEL_SELECTOR_PARAMETER.getKey());
   }

   private Optional<String> podFieldSelector() {
      return parameters.stringValue(POD_FIELD_SELECTOR_PARAMETER.getKey());
   }

   private Optional<String> container() {
      return parameters.stringValue(CONTAINER_PARAMETER.getKey());
   }

   private boolean verifySsl() {
      return parameters.booleanValue(VERIFY_SSL_PARAMETER.getKey()).orElse(VERIFY_SSL_PARAMETER.getValue());
   }

}
