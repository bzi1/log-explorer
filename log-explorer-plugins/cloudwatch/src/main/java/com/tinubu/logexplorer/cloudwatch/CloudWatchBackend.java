/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.cloudwatch;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.lang.validation.Validate.satisfies;
import static com.tinubu.logexplorer.core.search.query.QueryBuilder.nativeQuery;
import static io.github.resilience4j.core.IntervalFunction.ofExponentialBackoff;
import static java.time.Duration.ofMillis;
import static java.time.Duration.ofSeconds;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.StringUtils.isBlank;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.regex.Pattern;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.config.Configurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tinubu.commons.lang.util.Pair;
import com.tinubu.logexplorer.core.backend.Backend;
import com.tinubu.logexplorer.core.backend.BackendConfiguration;
import com.tinubu.logexplorer.core.backend.BackendInterruptionState;
import com.tinubu.logexplorer.core.config.Configuration.Parameters;
import com.tinubu.logexplorer.core.extension.parameter.ParameterDescription;
import com.tinubu.logexplorer.core.extension.parameter.completers.CacheCompleter;
import com.tinubu.logexplorer.core.lang.TypeConverter;
import com.tinubu.logexplorer.core.search.QueryDateRange;
import com.tinubu.logexplorer.core.search.SearchResult;
import com.tinubu.logexplorer.core.search.SearchResult.Attributes;
import com.tinubu.logexplorer.core.search.query.NativeQuery;
import com.tinubu.logexplorer.core.search.query.Query;
import com.tinubu.logexplorer.core.search.query.QuerySpecification;
import com.tinubu.logexplorer.core.search.query.SimpleQuery;
import com.tinubu.logexplorer.core.search.query.SimpleQuery.Option;
import com.tinubu.logexplorer.core.search.query.UniformQuery;

import io.github.resilience4j.core.IntervalFunction;
import io.github.resilience4j.retry.Retry;
import io.github.resilience4j.retry.RetryConfig;
import io.github.resilience4j.retry.RetryRegistry;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProviderChain;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProviderChain.Builder;
import software.amazon.awssdk.auth.credentials.ContainerCredentialsProvider;
import software.amazon.awssdk.auth.credentials.EnvironmentVariableCredentialsProvider;
import software.amazon.awssdk.auth.credentials.ProfileCredentialsProvider;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.auth.credentials.SystemPropertyCredentialsProvider;
import software.amazon.awssdk.auth.credentials.WebIdentityTokenFileCredentialsProvider;
import software.amazon.awssdk.awscore.exception.AwsServiceException;
import software.amazon.awssdk.awscore.internal.AwsErrorCode;
import software.amazon.awssdk.http.SdkHttpClient;
import software.amazon.awssdk.http.apache.ApacheHttpClient;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.cloudwatchlogs.CloudWatchLogsClient;
import software.amazon.awssdk.services.cloudwatchlogs.model.DescribeLogGroupsRequest;
import software.amazon.awssdk.services.cloudwatchlogs.model.DescribeLogGroupsResponse;
import software.amazon.awssdk.services.cloudwatchlogs.model.DescribeLogStreamsRequest;
import software.amazon.awssdk.services.cloudwatchlogs.model.DescribeLogStreamsResponse;
import software.amazon.awssdk.services.cloudwatchlogs.model.FilterLogEventsRequest;
import software.amazon.awssdk.services.cloudwatchlogs.model.FilterLogEventsResponse;
import software.amazon.awssdk.services.cloudwatchlogs.model.FilteredLogEvent;
import software.amazon.awssdk.services.cloudwatchlogs.model.LogGroup;
import software.amazon.awssdk.services.cloudwatchlogs.model.LogStream;

public class CloudWatchBackend implements Backend {

   private static final Logger logger = LoggerFactory.getLogger(CloudWatchBackend.class);

   /** Page size parameter definition. */
   private static final Pair<String, Integer> PAGE_SIZE_PARAMETER =
         Pair.of("cloudwatch.backend.page-size", 1000);
   /** Log group parameter definition. */
   private static final Pair<String, String> GROUP_PARAMETER = Pair.of("cloudwatch.backend.group", null);
   /** Stream prefix parameter definition. */
   private static final Pair<String, String> STREAM_PREFIX_PARAMETER =
         Pair.of("cloudwatch.backend.stream-prefix", null);
   /** Stream prefix parameter definition. */
   private static final Pair<String, String> STREAM_FILTER_PARAMETER =
         Pair.of("cloudwatch.backend.stream-filter", null);
   /** Follow mode interval parameter definition. */
   private static final Pair<String, Integer> FOLLOW_WAIT_INTERVAL_PARAMETER =
         Pair.of("cloudwatch.backend.follow-interval", 1000);
   /** AWS profile parameter definition. */
   private static final Pair<String, String> AWS_PROFILE_PARAMETER =
         Pair.of("cloudwatch.backend.aws-profile", null);
   /** Simple query attribute parameter definition. */
   private static final Pair<String, String> SIMPLE_QUERY_ATTRIBUTE_PARAMETER =
         Pair.of("cloudwatch.backend.simple-query-attribute", null);

   /** Maximum duration in milliseconds between each request in follow mode. */
   private static final int MAX_FOLLOW_WAIT_INTERVAL = 3600000;
   private static final int GROUP_CACHE_DURATION = 120;
   private static final int STREAM_CACHE_DURATION = 20;
   /** Maximum number of retries for retryable operation, must be greater than 0. */
   private static final int MAX_RETRIES = 6;
   /** Retry interval function. */
   private static final IntervalFunction RETRY_INTERVAL =
         ofExponentialBackoff(ofSeconds(1), 1.5, ofSeconds(3));

   private final CloudWatchLogsClient client;
   private final String group;
   private final String streamPrefix;
   private final Pattern streamFilter;
   private final int followWaitDuration;
   private final int pageSize;
   private String simpleQueryAttribute;

   public CloudWatchBackend(Parameters parameters, BackendConfiguration configuration, boolean debug) {
      notNull(parameters, "parameters");
      notNull(configuration, "configuration");
      satisfies(configuration.backendUris(),
                uris -> uris != null && uris.size() == 1,
                "backendUris",
                "must contain one and only one URI 'aws://<region-name>'");
      satisfies(configuration.backendUris(),
                uris -> uris.get(0).getScheme().equals("aws"),
                "backendUris",
                "must be at format 'aws://<region-name>'");

      logger.debug("Initialize CloudWatch client with '{}'", configuration);

      Configurator.setLevel("com.amazonaws", Boolean.TRUE.equals(debug) ? Level.DEBUG : Level.INFO);
      Configurator.setLevel("com.amazonaws.auth.profile.internal.BasicProfileConfigLoader", Level.ERROR);

      String region = configuration.backendUris().get(0).getHost();

      SdkHttpClient clientConfiguration = ApacheHttpClient
            .builder()
            .connectionTimeout(ofMillis(configuration.connectTimeout()))
            .socketTimeout(ofMillis(configuration.socketTimeout()))
            .build();

      client = CloudWatchLogsClient
            .builder()
            .region(Region.of(region))
            .credentialsProvider(credentialsProviderChain(configuration.authenticationUser(),
                                                          configuration.authenticationPassword(),
                                                          parameters
                                                                .stringValue(AWS_PROFILE_PARAMETER.getKey())
                                                                .orElse(AWS_PROFILE_PARAMETER.getValue())))
            .httpClient(clientConfiguration)
            .build();

      group = parameters.stringValue(GROUP_PARAMETER.getKey()).orElse(GROUP_PARAMETER.getValue());
      streamPrefix = parameters
            .stringValue(STREAM_PREFIX_PARAMETER.getKey())
            .orElse(STREAM_PREFIX_PARAMETER.getValue());
      streamFilter = parameters
            .patternValue(STREAM_FILTER_PARAMETER.getKey())
            .orElse(nullable(STREAM_FILTER_PARAMETER.getValue())
                          .map(TypeConverter::patternValue)
                          .orElse(null));
      this.followWaitDuration = satisfies(parameters
                                                .intValue(FOLLOW_WAIT_INTERVAL_PARAMETER.getKey())
                                                .orElse(FOLLOW_WAIT_INTERVAL_PARAMETER.getValue()),
                                          followWaitDuration -> followWaitDuration >= 0
                                                                && followWaitDuration
                                                                   <= MAX_FOLLOW_WAIT_INTERVAL,
                                          "followWaitDuration",
                                          String.format("must be included in [0, %d]",
                                                        MAX_FOLLOW_WAIT_INTERVAL));
      pageSize = parameters.intValue(PAGE_SIZE_PARAMETER.getKey()).orElse(PAGE_SIZE_PARAMETER.getValue());
      this.simpleQueryAttribute = parameters
            .stringValue(SIMPLE_QUERY_ATTRIBUTE_PARAMETER.getKey())
            .orElse(SIMPLE_QUERY_ATTRIBUTE_PARAMETER.getValue());

      if (isBlank(this.simpleQueryAttribute)) {
         logger.warn("Simple queries will be post-applied (low performance) because '{}' parameter not set",
                     SIMPLE_QUERY_ATTRIBUTE_PARAMETER.getKey());

      }

   }

   @Override
   public void search(BiConsumer<SearchResult, QuerySpecification> action,
                      QuerySpecification querySpecification,
                      QueryDateRange dateRange,
                      Long numberLines,
                      boolean followMode) {
      logger.debug("Search CloudWatch for query specification '{}' spanning '{}' (numberLines={} follow={})",
                   querySpecification,
                   dateRange,
                   numberLines,
                   followMode);

      if (numberLines != null) {
         logger.warn("numberLines is not supported by this backend");
      }

      if (!querySpecification.notQuerySet(NativeQuery.class).isEmpty()) {
         logger.warn("Native 'notQuery' queries are ignored by this backend");
      }

      querySpecification = querySpecification.mapQuerySet(this::translateQuery);

      String filterPattern = querySetCompound(querySpecification
                                                    .querySet(NativeQuery.class)
                                                    .stream()
                                                    .map(NativeQuery::queryString)
                                                    .collect(toList()));

      FilterLogEventsRequest.Builder request = FilterLogEventsRequest
            .builder()
            .logGroupName(group)
            .logStreamNames(logStreams(streamPrefix, streamFilter))
            .startTime(dateRange.fromDate().toInstant().toEpochMilli())
            .endTime(followMode ? null : dateRange.toDate().toInstant().toEpochMilli())
            .limit(pageSize)
            .filterPattern(filterPattern);

      String followFromEventId = null;
      Long followFromStartTime = null;
      do {
         boolean continuePagination = true;
         String nextToken = null;

         if (followFromStartTime != null) {
            request = request.startTime(followFromStartTime);
         }

         do {
            request.nextToken(nextToken);
            FilterLogEventsResponse result = filterLogEvents(request.build());
            nextToken = result.nextToken();

            List<FilteredLogEvent> events = dropSameMillisecondEvents(result.events(), followFromEventId);

            action.accept(mapEvents(events, group), querySpecification);

            if (nextToken == null) {
               continuePagination = false;
               if (followMode) {
                  try {
                     Thread.sleep(followWaitDuration);
                  } catch (InterruptedException e) {
                     Thread.currentThread().interrupt();
                  }
               }
            }

            if (followMode && events.size() > 0) {
               FilteredLogEvent lastEvent = events.get(events.size() - 1);
               followFromStartTime = lastEvent.timestamp();
               followFromEventId = lastEvent.eventId();
            }

         } while (!BackendInterruptionState.interrupting() && continuePagination);
      } while (!BackendInterruptionState.interrupting() && followMode);
   }

   /**
    * Translates specified queries to {@link NativeQuery} if possible, or returns original query.
    *
    * @param query query to translate
    *
    * @return translated query or original query
    */
   private Query translateQuery(Query query) {
      if (query instanceof SimpleQuery) {
         return translateSimpleQuery((SimpleQuery) query);
      } else if (query instanceof UniformQuery) {
         return translateUniformQuery((UniformQuery) query);
      } else {
         return query;
      }
   }

   private Query translateUniformQuery(UniformQuery query) {
      logger.debug(
            "Uniform queries will be post-applied (low performance) because this backend can't translate them");
      return query;
   }

   private Query translateSimpleQuery(SimpleQuery query) {
      if (!isBlank(simpleQueryAttribute)) {
         if (query.hasOption(Option.CASE_INSENSITIVE)) {
            logger.debug(
                  "Case insensitive simple queries will be post-applied (low performance) because this backend is case sensitive only");
            return query;
         } else {
            return nativeQuery(String.format("{%s=\"*%s*\"}",
                                             simpleQueryAttribute,
                                             escapeNativeQueryString(query.queryString())));
         }
      } else {
         return query;
      }
   }

   private String escapeNativeQueryString(String query) {
      return query.replace("\"", "\\\"");
   }

   private FilterLogEventsResponse filterLogEvents(FilterLogEventsRequest request) {
      return cloudWatchRetrier("filterLogEvents").executeSupplier(() -> client.filterLogEvents(request));
   }

   private static Retry cloudWatchRetrier(String call) {
      notBlank(call, "call");

      RetryRegistry registry = RetryRegistry.of(RetryConfig
                                                      .custom()
                                                      .maxAttempts(MAX_RETRIES)
                                                      .intervalFunction(RETRY_INTERVAL)
                                                      .retryOnException(CloudWatchBackend::isRetryableException)
                                                      .build());

      Retry retry = registry.retry(call);

      if (logger.isDebugEnabled()) {
         retry.getEventPublisher()
               .onRetry(event -> logger.debug("Retry call to '{}' # {}",
                                              event.getName(),
                                              event.getNumberOfRetryAttempts()));
      }
      return retry;
   }

   private static boolean isRetryableException(Throwable throwable) {
      if (throwable instanceof AwsServiceException) {
         AwsServiceException e = (AwsServiceException) throwable;

         return e.retryable() || e.isThrottlingException() || nullable(e.awsErrorDetails())
               .map(a -> AwsErrorCode.isRetryableErrorCode(a.errorCode()))
               .orElse(false);
      }

      return false;
   }

   /**
    * Compounds native query set depending on query nature :
    * <ul>
    *    <li>JSON query : {@code { (<query1>) && (<query2>) && ... }}</li>
    *    <li>Non JSON query : {@code <query1> <query2> ... }</li>
    * </ul>
    * Compound of a mix of JSON and not JSON queries is not supported
    *
    * @param querySet query set to compound
    *
    * @return compound query
    */
   private String querySetCompound(List<String> querySet) {
      StringBuilder compoundQuery = new StringBuilder();
      Boolean jsonQuery = null;
      for (String query : querySet) {
         if (!query.isBlank()) {
            String extractJsonQuery = extractJsonQuery(query);
            if ((jsonQuery == null || Boolean.TRUE.equals(jsonQuery)) && extractJsonQuery != null) {
               if (jsonQuery == null) {
                  compoundQuery.append("{ (").append(extractJsonQuery).append(")");
               } else {
                  compoundQuery.append(" && (").append(extractJsonQuery).append(")");
               }
               jsonQuery = true;
            } else if ((jsonQuery == null || Boolean.FALSE.equals(jsonQuery)) && extractJsonQuery == null) {
               if (jsonQuery == null) {
                  compoundQuery.append(query.trim());
               } else {
                  compoundQuery.append(" ").append(query.trim());
               }
               jsonQuery = false;
            } else {
               throw new IllegalStateException("Can't compound native JSON and pattern queries");
            }
         }
      }

      if (Boolean.TRUE.equals(jsonQuery)) {
         compoundQuery.append(" }");
      }

      logger.debug("Compound query = {}", compoundQuery);

      return compoundQuery.toString();
   }

   private String extractJsonQuery(String query) {
      String strippedQuery = query.strip();
      if (strippedQuery.startsWith("{") && strippedQuery.endsWith("}")) {
         return strippedQuery.substring(1, strippedQuery.length() - 1);
      } else {
         return null;
      }
   }

   private List<FilteredLogEvent> dropSameMillisecondEvents(List<FilteredLogEvent> events,
                                                            final String eventId) {
      if (eventId != null) {
         return events
               .stream()
               .dropWhile(event -> !event.eventId().equals(eventId))
               .dropWhile(event -> event.eventId().equals(eventId))
               .collect(toList());
      } else {
         return events;
      }
   }

   private SearchResult mapEvents(List<FilteredLogEvent> events, String group) {
      List<Attributes> attributes = events.stream().map(event -> mapEvent(event, group)).collect(toList());

      return new SearchResult(attributes);
   }

   private Attributes mapEvent(FilteredLogEvent event, String group) {
      return new Attributes()
            .withAttribute("timestamp", ofEpochMillis(event.timestamp()))
            .withAttribute("ingestionTimestamp", ofEpochMillis(event.ingestionTime()))
            .withAttribute("eventId", event.eventId())
            .withAttribute("message", event.message())
            .withAttribute("group", group)
            .withAttribute("stream", event.logStreamName());
   }

   private ZonedDateTime ofEpochMillis(Long timestamp) {
      return Instant.ofEpochMilli(timestamp).atZone(ZoneId.of("UTC"));
   }

   @Override
   public void close() {}

   private static AwsCredentialsProviderChain credentialsProviderChain(String accessKey,
                                                                       String secretKey,
                                                                       String awsProfile) {
      Builder credentialsProviderChain = AwsCredentialsProviderChain.builder();
      credentialsProviderChain.addCredentialsProvider(EnvironmentVariableCredentialsProvider.create());
      credentialsProviderChain.addCredentialsProvider(SystemPropertyCredentialsProvider.create());
      credentialsProviderChain.addCredentialsProvider(WebIdentityTokenFileCredentialsProvider.create());
      if (isBlank(awsProfile)) {
         logger.debug("Use 'default' profile or AWS_PROFILE environment variable for authentication");
         credentialsProviderChain.addCredentialsProvider(ProfileCredentialsProvider.create());
      } else {
         logger.debug("Use '{}' profile for authentication", awsProfile);
         credentialsProviderChain.addCredentialsProvider(ProfileCredentialsProvider.create(awsProfile));
      }
      credentialsProviderChain.addCredentialsProvider(ContainerCredentialsProvider.builder().build());

      if (accessKey != null && secretKey != null) {
         credentialsProviderChain.addCredentialsProvider(StaticCredentialsProvider.create(AwsBasicCredentials.create(
               accessKey,
               secretKey)));
      }

      return credentialsProviderChain.build();
   }

   @Override
   public List<ParameterDescription<?>> parameters() {
      return Arrays.asList(new ParameterDescription<>(PAGE_SIZE_PARAMETER.getKey(),
                                                      Integer.class,
                                                      PAGE_SIZE_PARAMETER.getValue(),
                                                      "request page size"),
                           new ParameterDescription<>(GROUP_PARAMETER.getKey(),
                                                      String.class,
                                                      GROUP_PARAMETER.getValue(),
                                                      "log group name").completer(new CacheCompleter(this::logGroups,
                                                                                                     GROUP_CACHE_DURATION)),
                           new ParameterDescription<>(STREAM_PREFIX_PARAMETER.getKey(),
                                                      String.class,
                                                      STREAM_PREFIX_PARAMETER.getValue(),
                                                      "log stream prefix").completer(new CacheCompleter(
                                 partial -> logStreams(partial, streamFilter),
                                 STREAM_CACHE_DURATION)),
                           new ParameterDescription<>(STREAM_FILTER_PARAMETER.getKey(),
                                                      String.class,
                                                      STREAM_FILTER_PARAMETER.getValue(),
                                                      "log stream Regexp filter"),
                           new ParameterDescription<>(FOLLOW_WAIT_INTERVAL_PARAMETER.getKey(),
                                                      Integer.class,
                                                      FOLLOW_WAIT_INTERVAL_PARAMETER.getValue(),
                                                      "interval duration in milliseconds between requests in follow mode"),
                           new ParameterDescription<>(AWS_PROFILE_PARAMETER.getKey(),
                                                      String.class,
                                                      AWS_PROFILE_PARAMETER.getValue(),
                                                      "AWS profile to force for authentication. 'default' profile, or AWS_PROFILE environment variable will be used by default"));
   }

   /**
    * Returns available group names.
    *
    * @param groupPrefix filter groups with log group prefix or {code null}
    *
    * @return available log group names
    */
   private List<String> logGroups(String groupPrefix) {
      List<String> groups = new ArrayList<>();
      String nextToken = null;
      do {
         DescribeLogGroupsRequest describeLogGroupsRequest = DescribeLogGroupsRequest
               .builder()
               .logGroupNamePrefix(groupPrefix == null || groupPrefix.isBlank() ? null : groupPrefix)
               .nextToken(nextToken)
               .build();

         DescribeLogGroupsResponse describeLogGroupsResult =
               cloudWatchRetrier("describeLogGroups").executeSupplier(() -> client.describeLogGroups(
                     describeLogGroupsRequest));
         groups.addAll(describeLogGroupsResult
                             .logGroups()
                             .stream()
                             .map(LogGroup::logGroupName)
                             .collect(toList()));
         nextToken = describeLogGroupsResult.nextToken();
      } while (nextToken != null);
      return groups;
   }

   /**
    * Returns available stream names for the current {@link #group}. If group name is not set, return an empty
    * list.
    *
    * @param streamPrefix filter streams with log stream prefix or {code null}
    * @param streamFilter filter streams with log stream pattern or {code null}
    *
    * @return available log stream names
    *
    * @implSpec Stream filter regexp is applied to resulting streams if it exists.
    */
   private List<String> logStreams(String streamPrefix, Pattern streamFilter) {
      List<String> streams = new ArrayList<>();

      if (group != null && !group.isBlank()) {
         String nextToken = null;
         do {
            String finalNextToken = nextToken;
            DescribeLogStreamsResponse describeLogGroupsResult =
                  cloudWatchRetrier("describeLogStreams").executeSupplier(() -> client.describeLogStreams(
                        DescribeLogStreamsRequest
                              .builder()
                              .logGroupName(group)
                              .nextToken(finalNextToken)
                              .build()));
            streams.addAll(describeLogGroupsResult
                                 .logStreams()
                                 .stream()
                                 .map(LogStream::logStreamName)
                                 .filter(stream -> streamPrefix == null || stream.startsWith(streamPrefix))
                                 .filter(stream -> streamFilter == null || streamFilter
                                       .matcher(stream)
                                       .matches())
                                 .collect(toList()));
            nextToken = describeLogGroupsResult.nextToken();
         } while (nextToken != null);
      }
      return streams;
   }
}
