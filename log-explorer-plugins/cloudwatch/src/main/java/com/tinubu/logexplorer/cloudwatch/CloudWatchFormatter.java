/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.cloudwatch;

import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE;
import static java.time.temporal.ChronoField.HOUR_OF_DAY;
import static java.time.temporal.ChronoField.MINUTE_OF_HOUR;
import static java.time.temporal.ChronoField.NANO_OF_SECOND;
import static java.time.temporal.ChronoField.SECOND_OF_MINUTE;
import static org.fusesource.jansi.Ansi.ansi;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.tinubu.commons.lang.util.Pair;
import com.tinubu.logexplorer.core.config.Configuration.Parameters;
import com.tinubu.logexplorer.core.extension.parameter.ParameterDescription;
import com.tinubu.logexplorer.core.extension.parameter.completers.BooleanCompleter;
import com.tinubu.logexplorer.core.formatter.Formatter;
import com.tinubu.logexplorer.core.search.SearchResult.Attributes;

public class CloudWatchFormatter implements Formatter {

   /** Date time formatter with 3 significant digits for fraction. */
   public static final DateTimeFormatter DATE_TIME_FORMATTER = new DateTimeFormatterBuilder()
         .parseCaseInsensitive()
         .append(ISO_LOCAL_DATE)
         .appendLiteral(' ')
         .append(new DateTimeFormatterBuilder()
                       .appendValue(HOUR_OF_DAY, 2)
                       .appendLiteral(':')
                       .appendValue(MINUTE_OF_HOUR, 2)
                       .optionalStart()
                       .appendLiteral(':')
                       .appendValue(SECOND_OF_MINUTE, 2)
                       .optionalStart()
                       .appendFraction(NANO_OF_SECOND, 3, 3, true)
                       .toFormatter())
         .toFormatter();
   /** Date time formatter with offset display. */
   private static final DateTimeFormatter TIME_OFFSET_FORMATTER = new DateTimeFormatterBuilder()
         .parseCaseInsensitive()
         .append(DATE_TIME_FORMATTER)
         .parseLenient()
         .appendOffsetId()
         .parseStrict()
         .toFormatter();
   /** Date time formatter with offset and zone id display. */
   private static final DateTimeFormatter TIME_ZONEID_FORMATTER = new DateTimeFormatterBuilder()
         .parseCaseInsensitive()
         .append(DATE_TIME_FORMATTER)
         .parseLenient()
         .appendOffsetId()
         .optionalStart()
         .appendLiteral('[')
         .parseCaseSensitive()
         .appendZoneRegionId()
         .appendLiteral(']')
         .parseStrict()
         .toFormatter();

   /** Display zone id parameter definition. */
   private static final Pair<String, Boolean> DISPLAY_ZONE_ID_PARAMETER =
         Pair.of("cloudwatch.formatter.display-zone-id", false);
   /** Single line message parameter definition. */
   private static final Pair<String, Boolean> SINGLE_LINE_MESSAGE_PARAMETER =
         Pair.of("cloudwatch.formatter.single-line-message", false);

   private static final Pattern SINGLE_LINE_MESSAGE_PATTERN = Pattern.compile("\\s*(?:\\r|\\r\\n|\\n)\\s*");

   private final ZoneId timezone;
   private final DateTimeFormatter dateTimeFormatter;
   private final boolean singleLineMessage;

   public CloudWatchFormatter(Parameters parameters, ZoneId timezone) {
      notNull(parameters, "parameters");
      this.timezone = notNull(timezone, "timezone");
      this.dateTimeFormatter = parameters
                                     .booleanValue(DISPLAY_ZONE_ID_PARAMETER.getKey())
                                     .orElse(DISPLAY_ZONE_ID_PARAMETER.getValue())
                               ? TIME_ZONEID_FORMATTER
                               : TIME_OFFSET_FORMATTER;
      this.singleLineMessage = parameters
            .booleanValue(SINGLE_LINE_MESSAGE_PARAMETER.getKey())
            .orElse(SINGLE_LINE_MESSAGE_PARAMETER.getValue());
   }

   @Override
   public void format(StringBuilder stringBuilder, Attributes attributes) {
      ZonedDateTime timestamp = (ZonedDateTime) attributes.get("timestamp");
      stringBuilder.append(timestamp
                                 .withZoneSameInstant(timezone)
                                 .truncatedTo(ChronoUnit.MILLIS).format(dateTimeFormatter));

      stringBuilder.append(ansi().a(" [").fgCyan().a(attributes.get("group")).reset().a("]"));
      stringBuilder.append(ansi().a(" ").fgCyan().a(attributes.get("stream")).reset());

      String message = (String) message(attributes.get("message"));

      stringBuilder.append(" ").append(message);
   }

   private Object message(Object value) {
      if (value instanceof String && singleLineMessage) {
         return SINGLE_LINE_MESSAGE_PATTERN
               .matcher(((String) value).strip())
               .replaceAll(ansi().a(" ").fgBrightRed().a("|").reset().a(" ").toString());
      } else {
         return value;
      }
   }

   @Override
   public List<ParameterDescription<?>> parameters() {
      return Arrays.asList(new ParameterDescription<>(DISPLAY_ZONE_ID_PARAMETER.getKey(),
                                                      Boolean.class,
                                                      DISPLAY_ZONE_ID_PARAMETER.getValue(),
                                                      "display zone id in addition to offset in timestamps").completer(
                                 new BooleanCompleter()),
                           new ParameterDescription<>(SINGLE_LINE_MESSAGE_PARAMETER.getKey(),
                                                      Boolean.class,
                                                      SINGLE_LINE_MESSAGE_PARAMETER.getValue(),
                                                      "display multi-line messages on a single line").completer(
                                 new BooleanCompleter()));
   }

   @Override
   public String displayString() {
      return CloudWatchFormatterService.FORMATTER_NAME;
   }

   @Override
   public String toString() {
      return new ToStringBuilder(this)
            .append("timezone", timezone)
            .append("dateTimeFormatter", dateTimeFormatter)
            .append("singleLineMessage", singleLineMessage)
            .toString();
   }
}
