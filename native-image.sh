#!/bin/sh

/Library/Java/JavaVirtualMachines/graalvm-ce-1.0.0-rc14/Contents/Home/bin/native-image \
  --no-server \
  -cp target/log-explorer-1.0.0.jar \
  "$@" \
  com.tinubu.logexplorer.LogExplorer

exit 0
--enable-url-protocols=http,https \
  --enable-http \
  --enable-https \
  --enable-all-security-services \
  --rerun-class-initialization-at-runtime=javax.net.ssl.SSLContext

HOM
