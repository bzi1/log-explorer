/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.core.lang;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.regex.Pattern;

import org.junit.jupiter.api.Test;

import com.tinubu.logexplorer.core.lang.TypeConverter.ConversionException;

class TypeConverterTest {

   @Test
   public void testPatternValueWhenPatternFormatString() {
      Pattern pattern = TypeConverter.patternValue("/a.*regexp/");
      assertThat(pattern.pattern()).isEqualTo("a.*regexp");
      assertThat(pattern.flags()).isEqualTo(0);
   }

   @Test
   public void testPatternValueWhenPatternFormatStringWithFlags() {
      Pattern pattern = TypeConverter.patternValue("/a.*regexp/dsmUcilux");
      assertThat(pattern.pattern()).isEqualTo("a.*regexp");
      assertThat(pattern.flags()).isEqualTo(Pattern.MULTILINE
                                            | Pattern.DOTALL
                                            | Pattern.CANON_EQ
                                            | Pattern.CASE_INSENSITIVE
                                            | Pattern.COMMENTS
                                            | Pattern.LITERAL
                                            | Pattern.UNICODE_CASE
                                            | Pattern.UNICODE_CHARACTER_CLASS
                                            | Pattern.UNIX_LINES);
   }

   @Test
   public void testPatternValueWhenPatternFormatStringWithUnknownFlags() {
      assertThatThrownBy(() -> TypeConverter.patternValue("/a.*regexp/msUciZxlmud"))
            .isInstanceOf(ConversionException.class)
            .hasMessage("Unknown 'Z' regexp flag character");
   }

   @Test
   public void testPatternValueWhenPatternFormatStringWithBadPattern() {
      assertThatThrownBy(() -> TypeConverter.patternValue("/(unfinished-group/"))
            .isInstanceOf(ConversionException.class)
            .hasMessage("Can't parse '(unfinished-group' regexp");
   }

   @Test
   public void testPatternValueWhenBadEscaping() {
      assertThatThrownBy(() -> TypeConverter.patternValue("/a/regexp/")).isInstanceOf(ConversionException.class);
      assertThatThrownBy(() -> TypeConverter.patternValue("/a\\\\/regexp/")).isInstanceOf(ConversionException.class);
      assertThatThrownBy(() -> TypeConverter.patternValue("\\/a regexp/")).isInstanceOf(ConversionException.class);
      assertThatThrownBy(() -> TypeConverter.patternValue("/a regexp\\/")).isInstanceOf(ConversionException.class);
   }

   @Test
   public void testPatternValueWhenEscaping() {
      Pattern pattern = TypeConverter.patternValue("/a reg\\exp/m");
      assertThat(pattern.pattern()).isEqualTo("a reg\\exp");
      assertThat(pattern.flags()).isEqualTo(Pattern.MULTILINE);

      pattern = TypeConverter.patternValue("/a reg\\/exp/");
      assertThat(pattern.pattern()).isEqualTo("a reg/exp");
      assertThat(pattern.flags()).isEqualTo(0);

      pattern = TypeConverter.patternValue("/a reg\\\\exp/");
      assertThat(pattern.pattern()).isEqualTo("a reg\\\\exp");
      assertThat(pattern.flags()).isEqualTo(0);

      pattern = TypeConverter.patternValue("/a reg\\\\\\/exp/");
      assertThat(pattern.pattern()).isEqualTo("a reg\\\\/exp");
      assertThat(pattern.flags()).isEqualTo(0);

      pattern = TypeConverter.patternValue("/a regexp\\\\/");
      assertThat(pattern.pattern()).isEqualTo("a regexp\\\\");
      assertThat(pattern.flags()).isEqualTo(0);
   }

   @Test
   public void testLenientPatternValueWhenSimpleString() {
      Pattern pattern = TypeConverter.lenientPatternValue("a.*regexp");
      assertThat(pattern.pattern()).isEqualTo("a.*regexp");
      assertThat(pattern.flags()).isEqualTo(0);
   }

   @Test
   public void testLenientPatternValueWhenPatternFormatStringWithBadPattern() {
      assertThatThrownBy(() -> TypeConverter.lenientPatternValue("(unfinished-group"))
            .isInstanceOf(ConversionException.class)
            .hasMessage("Can't parse '(unfinished-group' regexp");
   }

   @Test
   public void testLenientPatternValueWhenEscaping() {
      Pattern pattern = TypeConverter.lenientPatternValue("a reg\\exp");
      assertThat(pattern.pattern()).isEqualTo("a reg\\exp");
      assertThat(pattern.flags()).isEqualTo(0);

      pattern = TypeConverter.lenientPatternValue("a reg\\/exp");
      assertThat(pattern.pattern()).isEqualTo("a reg\\/exp");
      assertThat(pattern.flags()).isEqualTo(0);

      pattern = TypeConverter.lenientPatternValue("a reg\\\\exp");
      assertThat(pattern.pattern()).isEqualTo("a reg\\\\exp");
      assertThat(pattern.flags()).isEqualTo(0);

      pattern = TypeConverter.lenientPatternValue("a reg\\\\\\/exp");
      assertThat(pattern.pattern()).isEqualTo("a reg\\\\\\/exp");
      assertThat(pattern.flags()).isEqualTo(0);

      pattern = TypeConverter.lenientPatternValue("a regexp\\\\");
      assertThat(pattern.pattern()).isEqualTo("a regexp\\\\");
      assertThat(pattern.flags()).isEqualTo(0);
   }
}