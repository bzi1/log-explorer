/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.core.stacktrace;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.junit.jupiter.api.Test;

import com.tinubu.logexplorer.core.stacktrace.StackTrace.ExceptionType;

class StackTraceTest {

   static {
      ToStringBuilder.setDefaultStyle(ToStringStyle.SHORT_PREFIX_STYLE);
   }

   @Test
   public void visitStackTraceWithRootCauseVisitorWhenCausedByChain() {
      StackTrace stackTrace = StackTrace.parse(causedByStackTrace());

      Map<StackTraceException, ExceptionType> visitedExceptions = new LinkedHashMap<>();
      stackTrace.rootCauseExceptionVisitor(visitedExceptions::put);

      assertThat(visitedExceptions.keySet()).containsExactly(stackTrace.exceptions().get(2),
                                                             stackTrace.exceptions().get(1),
                                                             stackTrace.exceptions().get(0));
      assertThat(visitedExceptions.values()).containsExactly(new ExceptionType(ExceptionType.ROOT_CAUSE_EXCEPTION),
                                                             new ExceptionType(ExceptionType.WRAPPING_EXCEPTION),
                                                             new ExceptionType(ExceptionType.WRAPPING_EXCEPTION
                                                                               | ExceptionType.ACTUAL_EXCEPTION));
   }

   @Test
   public void visitStackTraceWithActualExceptionVisitorWhenCausedByChain() {
      StackTrace stackTrace = StackTrace.parse(causedByStackTrace());

      Map<StackTraceException, ExceptionType> visitedExceptions = new LinkedHashMap<>();
      stackTrace.actualExceptionVisitor(visitedExceptions::put);

      assertThat(visitedExceptions.keySet()).containsExactly(stackTrace.exceptions().get(0),
                                                             stackTrace.exceptions().get(1),
                                                             stackTrace.exceptions().get(2));
      assertThat(visitedExceptions.values()).containsExactly(new ExceptionType(ExceptionType.ACTUAL_EXCEPTION),
                                                             new ExceptionType(ExceptionType.CAUSE_EXCEPTION),
                                                             new ExceptionType(ExceptionType.CAUSE_EXCEPTION
                                                                               | ExceptionType.ROOT_CAUSE_EXCEPTION));
   }

   @Test
   public void visitStackTraceWithRootCauseVisitorWhenWrappedByChain() {
      StackTrace stackTrace = StackTrace.parse(wrappedByStackTrace());

      Map<StackTraceException, ExceptionType> visitedExceptions = new LinkedHashMap<>();
      stackTrace.rootCauseExceptionVisitor(visitedExceptions::put);

      assertThat(visitedExceptions.keySet()).containsExactly(stackTrace.exceptions().get(0),
                                                             stackTrace.exceptions().get(1),
                                                             stackTrace.exceptions().get(2));
      assertThat(visitedExceptions.values()).containsExactly(new ExceptionType(ExceptionType.ROOT_CAUSE_EXCEPTION),
                                                             new ExceptionType(ExceptionType.WRAPPING_EXCEPTION),
                                                             new ExceptionType(ExceptionType.WRAPPING_EXCEPTION
                                                                               | ExceptionType.ACTUAL_EXCEPTION));
   }

   @Test
   public void visitStackTraceWithActualExceptionVisitorWhenWrappedByChain() {
      StackTrace stackTrace = StackTrace.parse(wrappedByStackTrace());

      Map<StackTraceException, ExceptionType> visitedExceptions = new LinkedHashMap<>();
      stackTrace.actualExceptionVisitor(visitedExceptions::put);

      assertThat(visitedExceptions.keySet()).containsExactly(stackTrace.exceptions().get(2),
                                                             stackTrace.exceptions().get(1),
                                                             stackTrace.exceptions().get(0));
      assertThat(visitedExceptions.values()).containsExactly(new ExceptionType(ExceptionType.ACTUAL_EXCEPTION),
                                                             new ExceptionType(ExceptionType.CAUSE_EXCEPTION),
                                                             new ExceptionType(ExceptionType.CAUSE_EXCEPTION
                                                                               | ExceptionType.ROOT_CAUSE_EXCEPTION));
   }

   @Test
   public void visitStackTraceWithRootCauseVisitorWhenSimpleChain() {
      StackTrace stackTrace = StackTrace.parse(simpleStackTrace());

      Map<StackTraceException, ExceptionType> visitedExceptions = new LinkedHashMap<>();
      stackTrace.rootCauseExceptionVisitor(visitedExceptions::put);

      assertThat(visitedExceptions.keySet()).containsExactly(stackTrace.exceptions().get(0));
      assertThat(visitedExceptions.values()).containsExactly(new ExceptionType(ExceptionType.ROOT_CAUSE_EXCEPTION
                                                                               | ExceptionType.ACTUAL_EXCEPTION));
   }

   @Test
   public void visitStackTraceWithActualExceptionVisitorWhenSimpleChain() {
      StackTrace stackTrace = StackTrace.parse(simpleStackTrace());

      Map<StackTraceException, ExceptionType> visitedExceptions = new LinkedHashMap<>();
      stackTrace.actualExceptionVisitor(visitedExceptions::put);

      assertThat(visitedExceptions.keySet()).containsExactly(stackTrace.exceptions().get(0));
      assertThat(visitedExceptions.values()).containsExactly(new ExceptionType(ExceptionType.ACTUAL_EXCEPTION
                                                                               | ExceptionType.ROOT_CAUSE_EXCEPTION));
   }

   private String simpleStackTrace() {
      return "java.pkg.RootException: Root message line 1\n"
             + "    root message line 2\n"
             + "      at o.a.c.d.AtClass1.atMethod1(AtClass1.java:392)\n"
             + "      at org.pkg.AtClass2.atMethod2(AtClass2.java:491)\n"
             + "      ... extra message\n";
   }

   private String causedByStackTrace() {
      return simpleStackTrace()
             + "   Caused by: java.pkg.CauseException1: Cause1 message line 1\n"
             + "    cause1 message line 2\n"
             + "      at o.a.c.d.AtClass1.atMethod1(AtClass1.java:392)\n"
             + "      at org.pkg.AtClass2.atMethod2(AtClass2.java:491)\n"
             + "      ... extra message\n"
             + "   Caused by: java.pkg.CauseException2: Cause2 message line 1\n"
             + "    cause2 message line 2\n"
             + "      at o.a.c.d.AtClass1.atMethod1(AtClass1.java:392)\n"
             + "      at org.pkg.AtClass2.atMethod2(AtClass2.java:491)\n"
             + "      ... extra message\n";
   }

   private String wrappedByStackTrace() {
      return simpleStackTrace()
             + "   Wrapped by: java.pkg.WrappingException1: Wrapping1 message line 1\n"
             + "    wrapping1 message line 2\n"
             + "      at o.a.c.d.AtClass1.atMethod1(AtClass1.java:392)\n"
             + "      at org.pkg.AtClass2.atMethod2(AtClass2.java:491)\n"
             + "      ... extra message\n"
             + "   Wrapped by: java.pkg.WrappingException2: Wrapping2 message line 1\n"
             + "    wrapping2 message line 2\n"
             + "      at o.a.c.d.AtClass1.atMethod1(AtClass1.java:392)\n"
             + "      at org.pkg.AtClass2.atMethod2(AtClass2.java:491)\n"
             + "      ... extra message\n";
   }

}