/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.core.search;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.time.temporal.ChronoUnit;

import com.tinubu.logexplorer.core.backend.Backend;
import com.tinubu.logexplorer.core.export.Exporter;
import com.tinubu.logexplorer.core.parser.Parser;
import com.tinubu.logexplorer.core.progress.ProgressMeter;
import com.tinubu.logexplorer.core.search.query.QueryEngine;
import com.tinubu.logexplorer.core.search.query.QuerySpecification;

/**
 * Support search execution logic.
 *
 * @apiNote Class life-cycle is global, not per-search.
 */
public class Search {
   private final Backend backend;
   private final ProgressMeter progressMeter;
   private final Exporter exporter;
   private final Parser parser;
   private final QueryEngine uniformQueryEngine = new QueryEngine();

   public Search(Backend backend, ProgressMeter progressMeter, Parser parser, Exporter exporter) {
      this.backend = notNull(backend, "backendClient");
      this.progressMeter = notNull(progressMeter, "progressMeter");
      this.parser = notNull(parser, "parser");
      this.exporter = notNull(exporter, "exporter");
   }

   public void search(QuerySpecification querySpecification,
                      QueryDateRange dateRange,
                      Long numberLines,
                      boolean followMode) {
      notNull(querySpecification, "querySet");
      notNull(dateRange, "dateRange");

      dateRange = dateRange.truncate(ChronoUnit.MILLIS);

      try {
         backend.search(this::action,
                        querySpecification,
                        dateRange,
                        (numberLines == null || numberLines < 0) ? null : numberLines,
                        followMode);
      } catch (Exception e) {
         throw new IllegalStateException(String.format("Error in '%s' backend > %s",
                                                       backend.getClass().getSimpleName(),
                                                       e.getMessage()), e);
      }

      progressMeter.close();
   }

   private void action(SearchResult result, QuerySpecification querySpecification) {
      result = parser.parse(result);
      result = uniformQueryEngine.filter(result, querySpecification);
      progressMeter.handle(result);
      exporter.export(result);
   }

}
