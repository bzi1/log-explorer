/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.core.config;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

import java.net.URI;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.tinubu.logexplorer.core.lang.TypeConverter;
import com.tinubu.logexplorer.core.search.query.Query;

/**
 * Configurable elements.
 */
public interface Configuration {
   /** Backend URIs. */
   List<URI> backendUris();

   /** Backend connection timeout in ms. */
   Integer connectTimeout();

   /** Backend socket timeout in ms. */
   Integer socketTimeout();

   /** Optional backend authentication token. */
   String authenticationToken();

   /** Optional backend authentication user. */
   String authenticationUser();

   /** Optional backend authentication password. */
   String authenticationPassword();

   /** Debug mode flag. */
   Boolean debug();

   /** ANSI configuration. */
   AnsiFlag ansi();

   /** Parser configuration. */
   String parser();

   /** Formatter configuration to display results. */
   Formatter formatter();

   /** Progress meter configuration. */
   ProgressMeterFlag progress();

   /** Query string that must match. */
   QueryGroup query();

   /** Query string that must not match. */
   QueryGroup notQuery();

   /** Timezone used for display. */
   ZoneId timezone();

   /** Backend. */
   String backend();

   /** Custom parameters for plugins. */
   Parameters parameters();

   /** Number of lines to return. */
   Long numberLines();

   /** Console command aliases. */
   Aliases aliases();

   /** ANSI configuration values. */
   enum AnsiFlag {ALWAYS, NEVER, AUTO}

   /** Progress meter configuration values. */
   enum ProgressMeterFlag {ALWAYS, NEVER, AUTO}

   class Formatter {
      /** Formatter name (e.g.: raw). */
      private String name;
      /** Optional template content depending on {@code name}. */
      private String template;

      public Formatter(String name, String template) {
         this.name = notBlank(name, "name");
         this.template = nullable(template).map(s -> notBlank(s, "template")).orElse(null);
      }

      public Formatter(String name) {
         this(name, null);
      }

      public String name() {
         return name;
      }

      public String template() {
         return template;
      }

      @Override
      public String toString() {
         return new ToStringBuilder(this).append("name", name).append("template", template).toString();
      }
   }

   /**
    * Map of backend queries indexed by arbitrary group name.
    * <p>
    * Queries are represented as map so that they can be merged in inheritance and between configuration
    * layers. The query index meaning is left to the user context.
    */
   class QueryGroup extends HashMap<String, List<Query>> {

      public QueryGroup() {
      }

      /**
       * Copy constructor.
       *
       * @param queryGroup query group to copy
       */
      public QueryGroup(Map<? extends String, List<Query>> queryGroup) {
         super(queryGroup);
      }

      /**
       * Returns a flat version of the query group.
       * All groups are flattened into a single list in arbitrary order.
       *
       * @return flattened query list, never {@code null}
       */
      public List<Query> flatten() {
         return this
               .values()
               .stream()
               .reduce((l1, l2) -> Stream.concat(l1.stream(), l2.stream()).collect(toList()))
               .orElse(new ArrayList<>());
      }

      /**
       * Alias to express the merging mechanism of query groups.
       *
       * @param queryGroup query group to merge into this map
       */
      public void merge(Map<? extends String, List<Query>> queryGroup) {
         super.putAll(queryGroup);
      }
   }

   /**
    * Map of plugin parameters.
    */
   class Parameters extends HashMap<String, Object> {

      public Parameters() {
         super();
      }

      public Parameters(Map<? extends String, ?> parameters) {
         super(parameters);
      }

      public Parameters withParameter(String key, Object value) {
         put(key, value);
         return this;
      }

      /**
       * Alias to express the merging mechanism of parameters.
       *
       * @param parameters parameters to merge into this map
       */
      public void merge(Map<? extends String, ?> parameters) {
         super.putAll(parameters);
      }

      public Optional<String> stringValue(String parameter) {
         return nullable(get(notBlank(parameter,
                                      "parameter must not be blank"))).map(TypeConverter::stringValue);
      }

      public List<String> stringListValue(String parameter) {
         return nullable(get(notBlank(parameter, "parameter must not be blank")))
               .map(TypeConverter::stringListValue)
               .orElse(emptyList());
      }

      public Optional<Integer> intValue(String parameter) {
         return nullable(get(notBlank(parameter,
                                      "parameter must not be blank"))).map(TypeConverter::intValue);
      }

      public List<Integer> intListValue(String parameter) {
         return nullable(get(notBlank(parameter, "parameter must not be blank")))
               .map(TypeConverter::intListValue)
               .orElse(emptyList());
      }

      public Optional<Long> longValue(String parameter) {
         return nullable(get(notBlank(parameter,
                                      "parameter must not be blank"))).map(TypeConverter::longValue);
      }

      public List<Long> longListValue(String parameter) {
         return nullable(get(notBlank(parameter, "parameter must not be blank")))
               .map(TypeConverter::longListValue)
               .orElse(emptyList());
      }

      public Optional<Float> floatValue(String parameter) {
         return nullable(get(notBlank(parameter,
                                      "parameter must not be blank"))).map(TypeConverter::floatValue);
      }

      public List<Float> floatListValue(String parameter) {
         return nullable(get(notBlank(parameter, "parameter must not be blank")))
               .map(TypeConverter::floatListValue)
               .orElse(emptyList());
      }

      public Optional<Double> doubleValue(String parameter) {
         return nullable(get(notBlank(parameter,
                                      "parameter must not be blank"))).map(TypeConverter::doubleValue);
      }

      public List<Double> doubleListValue(String parameter) {
         return nullable(get(notBlank(parameter, "parameter must not be blank")))
               .map(TypeConverter::doubleListValue)
               .orElse(emptyList());
      }

      public Optional<Boolean> booleanValue(String parameter) {
         return nullable(get(notBlank(parameter,
                                      "parameter must not be blank"))).map(TypeConverter::booleanValue);
      }

      public List<Boolean> booleanListValue(String parameter) {
         return nullable(get(notBlank(parameter, "parameter must not be blank")))
               .map(TypeConverter::booleanListValue)
               .orElse(emptyList());
      }

      public Optional<Pattern> patternValue(String parameter) {
         return nullable(get(notBlank(parameter,
                                      "parameter must not be blank"))).map(TypeConverter::patternValue);
      }

      public List<Pattern> patternListValue(String parameter) {
         return nullable(get(notBlank(parameter, "parameter must not be blank")))
               .map(TypeConverter::patternListValue)
               .orElse(emptyList());
      }
   }

   /**
    * Alias map.
    */
   class Aliases extends HashMap<String, String> {

      public Aliases() {
      }

      /**
       * Copy constructor.
       *
       * @param aliases aliases to copy
       */
      public Aliases(Map<? extends String, String> aliases) {
         super(aliases);
      }

      /**
       * Alias to express the merging mechanism of query groups.
       *
       * @param aliases aliases to merge
       */
      public void merge(Map<? extends String, String> aliases) {
         super.putAll(aliases);
      }

      /**
       * Adds an alias.
       *
       * @param alias alias name
       * @param target alias target
       *
       * @return this query group
       */
      public Aliases withAlias(String alias, String target) {
         put(alias, target);
         return this;
      }

   }

}
