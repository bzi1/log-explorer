/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.core.lang;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.time.DateTimeException;
import java.time.ZoneId;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import java.util.stream.Stream;

import com.tinubu.commons.lang.mapper.EnumMapper;

/**
 * General purpose, featureful, {@link Object} to type converter.
 * <p>
 * All converters returns {@code null} if specified value is {@code null}.
 * All converters support at least the conversion from their own type and {@link String}.
 */
@SuppressWarnings({ "java:S1168" /* Return an empty collection instead of null */ })
public final class TypeConverter {

   /** Separator to use to convert a string into a list. */
   private static final String LIST_SEPARATOR = ",";
   private static final Pattern REGEXP_UNESCAPE_SLASH_PATTERN = Pattern.compile("((?:\\\\\\\\)*)\\\\/");
   private static final Pattern REGEXP_UNESCAPE_BACKSLASH_PATTERN = Pattern.compile("\\\\\\\\");
   private static final Pattern REGEXP_PATTERN = Pattern.compile("/((?:\\\\.|[^\\\\/])*)/([a-zA-Z]*)");

   private TypeConverter() {}

   public static class ConversionException extends RuntimeException {
      public ConversionException(String message) {
         super(message);
      }

      public ConversionException(String message, Throwable cause) {
         super(message, cause);
      }
   }

   /**
    * {@link Collection} of {@link String} values are supported and are returned as a {@value LIST_SEPARATOR}-
    * separated items string.
    */
   @SuppressWarnings("unchecked")
   public static String stringValue(Object value) {
      if (value == null) {
         return null;
      }

      if (value instanceof Collection) {
         return ((Collection<String>) value)
               .stream()
               .map(TypeConverter::stringValue)
               .collect(joining(LIST_SEPARATOR));
      } else {
         return String.valueOf(value);
      }
   }

   public static List<String> stringListValue(Object value) {
      if (value == null) {
         return null;
      }
      return listValue(value, String.class, TypeConverter::stringValue);
   }

   public static Integer intValue(Object value) {
      if (value == null) {
         return null;
      }
      if (value instanceof Integer) {
         return (Integer) value;
      } else {
         try {
            return Integer.parseInt(stringValue(value));
         } catch (NumberFormatException e) {
            throw new ConversionException(String.format("Can't convert '%s' int", value), e);
         }

      }
   }

   public static List<Integer> intListValue(Object value) {
      if (value == null) {
         return null;
      }
      return listValue(value, Integer.class, TypeConverter::intValue);
   }

   public static Long longValue(Object value) {
      if (value == null) {
         return null;
      }
      if (value instanceof Long) {
         return (Long) value;
      } else {
         try {
            return Long.parseLong(stringValue(value));
         } catch (NumberFormatException e) {
            throw new ConversionException(String.format("Can't convert '%s' long", value), e);
         }
      }
   }

   public static List<Long> longListValue(Object value) {
      if (value == null) {
         return null;
      }
      return listValue(value, Long.class, TypeConverter::longValue);
   }

   public static Float floatValue(Object value) {
      if (value == null) {
         return null;
      }
      if (value instanceof Float) {
         return (Float) value;
      } else {
         try {
            return Float.parseFloat(stringValue(value));
         } catch (NumberFormatException e) {
            throw new ConversionException(String.format("Can't convert '%s' float", value), e);
         }

      }
   }

   public static List<Float> floatListValue(Object value) {
      if (value == null) {
         return null;
      }
      return listValue(value, Float.class, TypeConverter::floatValue);
   }

   public static Double doubleValue(Object value) {
      if (value == null) {
         return null;
      }
      if (value instanceof Double) {
         return (Double) value;
      } else {
         try {
            return Double.parseDouble(stringValue(value));
         } catch (NumberFormatException e) {
            throw new ConversionException(String.format("Can't convert '%s' double", value), e);
         }
      }
   }

   public static List<Double> doubleListValue(Object value) {
      if (value == null) {
         return null;
      }
      return listValue(value, Double.class, TypeConverter::doubleValue);
   }

   public static Boolean booleanValue(Object value) {
      if (value == null) {
         return null;
      }
      if (value instanceof Boolean) {
         return (Boolean) value;
      } else {
         switch (stringValue(value).toLowerCase()) {
            case "true":
            case "yes":
            case "on":
            case "1":
               return true;
            case "false":
            case "no":
            case "off":
            case "0":
               return false;
            default:
               throw new ConversionException(String.format("Can't convert '%s' boolean", value));
         }
      }
   }

   public static List<Boolean> booleanListValue(Object value) {
      if (value == null) {
         return null;
      }
      return listValue(value, Boolean.class, TypeConverter::booleanValue);
   }

   /**
    * @throws ConversionException if value can't be converted to {@link ZoneId}
    */
   public static ZoneId zoneIdValue(Object value) {
      if (value == null) {
         return null;
      }
      if (value instanceof ZoneId) {
         return (ZoneId) value;
      } else if (value instanceof String) {
         if ("system".equals(((String) value).toLowerCase())) {
            return ZoneId.systemDefault();
         } else {
            try {
               return ZoneId.of((String) value);
            } catch (DateTimeException e) {
               throw new ConversionException(String.format("Can't convert '%s' zone id > %s",
                                                           value,
                                                           e.getMessage()));
            }
         }
      } else {
         throw new ConversionException(String.format("Can't convert '%s' zone id", value));
      }
   }

   /**
    * @throws ConversionException if value can't be converted to {@link ZoneId}
    */
   public static List<ZoneId> zonedIdListValue(Object value) {
      if (value == null) {
         return null;
      }
      return listValue(value, ZoneId.class, TypeConverter::zoneIdValue);
   }

   /**
    * @throws ConversionException if value can't be converted to {@link URL}
    */
   public static URL urlValue(Object value) {
      if (value == null) {
         return null;
      }

      if (value instanceof URL) {
         return (URL) value;
      } else if (value instanceof URI) {
         try {
            return ((URI) value).toURL();
         } catch (MalformedURLException e) {
            throw new ConversionException(String.format("Can't convert '%s' URL", value), e);
         }
      } else try {
         return new URL(stringValue(value));
      } catch (MalformedURLException e) {
         throw new ConversionException(String.format("Can't convert '%s' URL", value), e);
      }
   }

   /**
    * @throws ConversionException if value can't be converted to {@link URL}
    */
   public static List<URL> urlListValue(Object value) {
      if (value == null) {
         return null;
      }
      return listValue(value, URL.class, TypeConverter::urlValue);
   }

   /**
    * @throws ConversionException if value can't be converted to {@link URI}
    */
   public static URI uriValue(Object value) {
      if (value == null) {
         return null;
      }

      if (value instanceof URI) {
         return (URI) value;
      } else {
         try {
            return new URI(stringValue(value));
         } catch (URISyntaxException e) {
            throw new ConversionException(String.format("Can't convert '%s' URI", value), e);
         }
      }
   }

   /**
    * @throws ConversionException if value can't be converted to {@link URI}
    */
   public static List<URI> uriListValue(Object value) {
      if (value == null) {
         return null;
      }
      return listValue(value, URI.class, TypeConverter::uriValue);
   }

   /**
    * Convert a value to a {@link Pattern}.
    *
    * @param value regexp string to convert with default flags or {@code /<regexp>/[<flags>]} format
    *       with flags :
    *       <ul>
    *       <li>{@code m} : {@link Pattern#MULTILINE}</li>
    *       <li>{@code s} : {@link Pattern#DOTALL}</li>
    *       <li>{@code c} : {@link Pattern#CANON_EQ}</li>
    *       <li>{@code i} : {@link Pattern#CASE_INSENSITIVE}</li>
    *       <li>{@code x} : {@link Pattern#COMMENTS}</li>
    *       <li>{@code l} : {@link Pattern#LITERAL}</li>
    *       <li>{@code u} : {@link Pattern#UNICODE_CASE}</li>
    *       <li>{@code U} : {@link Pattern#UNICODE_CHARACTER_CLASS}</li>
    *       <li>{@code d} : {@link Pattern#UNIX_LINES}</li>
    *       </ul>
    *       {@code /} should be escaped (you will have to double {@code \\} in Java strings) :
    *       <ul>
    *          <li>{@code /my\/regexp/ -> pattern('my/regexp')}</li>
    *          <li>{@code \/my/reg\\/exp/ -> pattern('/my/reg\/exp/')}</li>
    *          <li>{@code my\/reg/exp -> pattern('my/reg/exp')}</li>
    *       </ul>
    *
    * @throws ConversionException if value can't be converted to {@link Pattern}
    */
   public static Pattern patternValue(Object value) {
      if (value == null) {
         return null;
      }

      if (value instanceof Pattern) {
         return (Pattern) value;
      } else {
         String stringValue = stringValue(value);
         int flags = 0;
         Matcher stringValueMatcher = REGEXP_PATTERN.matcher(stringValue);
         if (stringValueMatcher.matches()) {
            stringValue = stringValueMatcher.group(1);
            for (char flag : stringValueMatcher.group(2).toCharArray()) {
               try {
                  flags |= RegexpFlag.fromFlagCharacter(flag).flag();
               } catch (IllegalArgumentException e) {
                  throw new ConversionException(e.getMessage());
               }
            }
         } else {
            throw new ConversionException(String.format("Can't parse '%s' regexp", stringValue));
         }
         try {
            return Pattern.compile(unescapePattern(stringValue), flags);
         } catch (PatternSyntaxException e) {
            throw new ConversionException(String.format("Can't parse '%s' regexp", stringValue), e);
         }
      }
   }

   /**
    * Unescapes a pattern format with possibly escaped {@code /} and {@code \}.
    *
    * @implNote Must unescape slashes only if preceded by an odd number of back-slashes.
    */
   private static String unescapePattern(String escapedValue) {
      if (escapedValue == null) {
         return null;
      }
      return REGEXP_UNESCAPE_SLASH_PATTERN.matcher(escapedValue).replaceAll("$1/");
   }

   public static Pattern lenientPatternValue(Object value) {
      try {
         return patternValue(value);
      } catch (ConversionException e) {
         String stringValue = stringValue(value);
         try {
            return Pattern.compile(stringValue);
         } catch (PatternSyntaxException pse) {
            throw new ConversionException(String.format("Can't parse '%s' regexp", stringValue), pse);
         }
      }
   }

   /**
    * @throws ConversionException if value can't be converted to {@link Pattern}
    */
   public static List<Pattern> patternListValue(Object value) {
      if (value == null) {
         return null;
      }
      return listValue(value, Pattern.class, TypeConverter::patternValue);
   }

   public static List<Pattern> lenientPatternListValue(Object value) {
      if (value == null) {
         return null;
      }
      return listValue(value, Pattern.class, TypeConverter::lenientPatternValue);
   }

   @SuppressWarnings("unchecked")
   private static <T> List<T> listValue(Object value, Class<T> valueType, Function<Object, T> mapper) {
      if (value == null) {
         return null;
      } else if (value instanceof String) {
         return Stream.of(((String) value).split(LIST_SEPARATOR)).map(mapper).collect(toList());
      } else if (valueType.isAssignableFrom(value.getClass())) {
         return Collections.singletonList(valueType.cast(value));
      } else if (value instanceof List) {
         return ((List<Object>) value).stream().map(mapper).collect(toList());
      } else {
         throw new IllegalStateException(String.format("Can't convert '%s'", value));
      }
   }

   public enum RegexpFlag {
      MULTILINE('m', Pattern.MULTILINE),
      DOTALL('s', Pattern.DOTALL),
      CANON_EQ('c', Pattern.CANON_EQ),
      CASE_INSENSITIVE('i', Pattern.CASE_INSENSITIVE),
      COMMENTS('x', Pattern.COMMENTS),
      LITERAL('l', Pattern.LITERAL),
      UNICODE_CASE('u', Pattern.UNICODE_CASE),
      UNICODE_CHARACTER_CLASS('U', Pattern.UNICODE_CHARACTER_CLASS),
      UNIX_LINES('d', Pattern.UNIX_LINES);

      private static final EnumMapper<RegexpFlag, Character> flagCharacterMapper =
            new EnumMapper<>(RegexpFlag.class, RegexpFlag::flagCharacter);
      private static final EnumMapper<RegexpFlag, Integer> flagMapper =
            new EnumMapper<>(RegexpFlag.class, RegexpFlag::flag);

      private final char flagCharacter;
      private final int flag;

      RegexpFlag(Character flagCharacter, int flag) {
         this.flagCharacter = flagCharacter;
         this.flag = flag;
      }

      public static RegexpFlag fromFlagCharacter(char flagCharacter) {
         return nullable(flagCharacterMapper.map(flagCharacter)).orElseThrow(() -> new IllegalArgumentException(
               String.format("Unknown '%c' regexp flag character", flagCharacter)));
      }

      public static RegexpFlag fromFlag(int flag) {
         return nullable(flagMapper.map(flag)).orElseThrow(() -> new IllegalArgumentException(String.format(
               "Unknown '%d' regexp flag",
               flag)));
      }

      /**
       * Generates a string of flag characters from a {@link Pattern}'s flags bitset.
       *
       * @param pattern regexp pattern
       *
       * @return a string of zero, one or more regexp flag characters
       */
      public static String flagString(Pattern pattern) {
         StringBuilder flags = new StringBuilder();
         for (RegexpFlag regexpFlag : values()) {
            if ((pattern.flags() & regexpFlag.flag) != 0) { flags.append(regexpFlag.flagCharacter); }
         }
         return flags.toString();
      }

      public char flagCharacter() {
         return flagCharacter;
      }

      public int flag() {
         return flag;
      }
   }

}
