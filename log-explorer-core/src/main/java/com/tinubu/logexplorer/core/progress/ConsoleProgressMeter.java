/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.core.progress;

import static org.fusesource.jansi.Ansi.Color.YELLOW;
import static org.fusesource.jansi.Ansi.ansi;

import java.io.PrintStream;
import java.io.PrintWriter;

import org.fusesource.jansi.Ansi.Attribute;

import com.tinubu.logexplorer.core.search.SearchResult;

public class ConsoleProgressMeter implements ProgressMeter {
   private long resultCount = 0;
   private long startTime;
   private final PrintStream printStream;
   private final PrintWriter out;

   public ConsoleProgressMeter(PrintStream printStream) {
      this.printStream = printStream;
      out = new PrintWriter(printStream);
      startTime = System.currentTimeMillis();
   }

   /**
    * @implSpec {@link SearchResult#total()} is < 0 means the backend does not support the feature.
    */
   public void handle(SearchResult result) {
      resultCount += result.count();
      long currentTime = System.currentTimeMillis();

      if (result.total() == null) {
         out.print(ansi()
                         .cursorToColumn(0)
                         .eraseLine()
                         .a("  ")
                         .a("(")
                         .a(resultCount)
                         .a(" lines) ")
                         .a(Attribute.INTENSITY_FAINT)
                         .a((resultCount * 1000) / (currentTime - startTime))
                         .a(" lines/s")
                         .newline()
                         .cursorUpLine()
                         .reset());
      } else {
         out.print(ansi()
                         .cursorToColumn(0)
                         .eraseLine()
                         .a("  ")
                         .fg(YELLOW)
                         .a(result.total() == 0 ? 100 : resultCount * 100 / result.total())
                         .a("% ")
                         .reset()
                         .a("(")
                         .a(resultCount)
                         .a("/")
                         .a(result.total())
                         .a(" lines) ")
                         .a(Attribute.INTENSITY_FAINT)
                         .a((resultCount * 1000) / (currentTime - startTime))
                         .a(" lines/s")
                         .newline()
                         .cursorUpLine()
                         .reset());
      }

      out.flush();
   }

   @Override
   public void close() {
      out.println();
      out.flush();
   }
}
