/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.core.config;

import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.Optional.ofNullable;

import java.net.URI;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import com.tinubu.logexplorer.core.config.Configuration.Aliases;
import com.tinubu.logexplorer.core.config.Configuration.AnsiFlag;
import com.tinubu.logexplorer.core.config.Configuration.Formatter;
import com.tinubu.logexplorer.core.config.Configuration.Parameters;
import com.tinubu.logexplorer.core.config.Configuration.ProgressMeterFlag;
import com.tinubu.logexplorer.core.config.Configuration.QueryGroup;

public class ConfigurationBuilder {
   private List<URI> backendUris = new ArrayList<>();
   private Integer connectTimeout = 5000;
   private Integer socketTimeout = 10000;
   private String authenticationToken;
   private String authenticationUser;
   private String authenticationPassword;
   private Boolean debug = false;
   private AnsiFlag ansi = AnsiFlag.AUTO;
   private String parser = "noop";
   private Formatter formatter = new Formatter("raw");
   private ProgressMeterFlag progress = ProgressMeterFlag.AUTO;
   private QueryGroup query = new QueryGroup();
   private QueryGroup notQuery = new QueryGroup();
   private ZoneId timezone = ZoneId.systemDefault();
   private String backend = "noop";
   private Parameters parameters = new Parameters();
   private Long numberLines;
   private Aliases aliases = new Aliases();

   /**
    * Chains configuration logic.
    *
    * @param configuration configuration ignored if {@code null}
    *
    * @return this builder
    */
   public ConfigurationBuilder chain(Configuration configuration) {
      if (configuration != null) {
         ofNullable(configuration.backendUris()).ifPresent(backendUris -> this.backendUris = backendUris);
         ofNullable(configuration.connectTimeout()).ifPresent(connectTimeout -> this.connectTimeout =
               connectTimeout);
         ofNullable(configuration.socketTimeout()).ifPresent(socketTimeout -> this.socketTimeout =
               socketTimeout);
         ofNullable(configuration.authenticationToken()).ifPresent(authenticationToken ->
                                                                         this.authenticationToken =
                                                                               authenticationToken);
         ofNullable(configuration.authenticationUser()).ifPresent(authenticationUser ->
                                                                        this.authenticationUser =
                                                                              authenticationUser);
         ofNullable(configuration.authenticationPassword()).ifPresent(authenticationPassword ->
                                                                            this.authenticationPassword =
                                                                                  authenticationPassword);
         ofNullable(configuration.debug()).ifPresent(debug -> this.debug = debug);
         ofNullable(configuration.ansi()).ifPresent(ansi -> this.ansi = ansi);
         ofNullable(configuration.parser()).ifPresent(parser -> this.parser = parser);
         ofNullable(configuration.formatter()).ifPresent(formatter -> this.formatter = formatter);
         ofNullable(configuration.progress()).ifPresent(progress -> this.progress = progress);
         ofNullable(configuration.query()).ifPresent(query -> this.query.merge(query));
         ofNullable(configuration.notQuery()).ifPresent(notQuery -> this.notQuery.merge(notQuery));
         ofNullable(configuration.timezone()).ifPresent(timezone -> this.timezone = timezone);
         ofNullable(configuration.backend()).ifPresent(backend -> this.backend = backend);
         ofNullable(configuration.parameters()).ifPresent(parameters -> this.parameters.merge(parameters));
         ofNullable(configuration.numberLines()).ifPresent(numberLines -> this.numberLines = numberLines);
         ofNullable(configuration.aliases()).ifPresent(aliases -> this.aliases.merge(aliases));
      }

      return this;
   }

   /**
    * Convenience alias when configuration is optional.
    *
    * @param configuration optional configuration
    */
   @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
   public <T extends Configuration> ConfigurationBuilder chain(Optional<T> configuration) {
      return chain(notNull(configuration, "configuration").orElse(null));
   }

   public <T extends Configuration> ConfigurationBuilder chains(Stream<T> configurations) {
      ofNullable(configurations).ifPresent(configs -> configs.forEach(this::chain));
      return this;
   }

   /**
    * Convenience alias when configuration is optional.
    *
    * @param configurations optional configurations
    */
   @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
   public <T extends Configuration> ConfigurationBuilder chains(Optional<Stream<T>> configurations) {
      chains(notNull(configurations, "configurations").orElse(null));
      return this;
   }

   /** Builds the configuration from resolved chain. */
   public Configuration build() {
      return new DirectConfiguration()
            .withBackendUris(backendUris)
            .withConnectTimeout(connectTimeout)
            .withSocketTimeout(socketTimeout)
            .withAuthenticationToken(authenticationToken)
            .withAuthenticationUser(authenticationUser)
            .withAuthenticationPassword(authenticationPassword)
            .withDebug(debug)
            .withAnsi(ansi)
            .withParser(parser)
            .withFormatter(formatter)
            .withProgress(progress)
            .withQuery(query)
            .withNotQuery(notQuery)
            .withTimezone(timezone)
            .withBackend(backend)
            .withParameters(parameters)
            .withNumberLines(numberLines)
            .withAliases(aliases);
   }

}
