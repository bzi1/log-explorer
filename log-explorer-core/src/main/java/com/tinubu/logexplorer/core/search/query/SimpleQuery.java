/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.core.search.query;

import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.logexplorer.core.lang.TypeConverter.stringValue;
import static java.util.Collections.emptySet;

import java.util.Objects;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.tinubu.logexplorer.core.search.SearchResult.Attributes;

/**
 * Simple query represented by a simple sentence to match in logs.
 * <p>
 * Match is partial, case-sensitive.
 */
public class SimpleQuery implements Query {

   private final String query;
   private final Set<Option> options;

   private SimpleQuery(String query, Set<Option> options) {
      this.query = notBlank(query, "query");
      this.options = notNull(options, "options");
   }

   public static SimpleQuery of(String query) {
      return new SimpleQuery(query, emptySet());
   }

   public static SimpleQuery of(String query, Set<Option> options) {
      return new SimpleQuery(query, options);
   }

   @Override
   public boolean match(Attributes attributes) {
      return attributes.values().stream().anyMatch(this::matchAttribute);
   }

   private boolean matchAttribute(Object attribute) {
      if (attribute instanceof Attributes) {
         return ((Attributes) attribute).values().stream().anyMatch(this::matchAttribute);
      } else {
         if (hasOption(Option.CASE_INSENSITIVE)) {
            return StringUtils.containsIgnoreCase(stringValue(attribute), query);
         } else {
            return StringUtils.contains(stringValue(attribute), query);
         }
      }
   }

   @Override
   public String queryString() {
      return query;
   }

   public Set<Option> options() {
      return options;
   }

   public boolean hasOption(Option option) {
      return options.contains(notNull(option, "option"));
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      SimpleQuery query1 = (SimpleQuery) o;
      return Objects.equals(query, query1.query) && Objects.equals(options, query1.options);
   }

   @Override
   public int hashCode() {
      return Objects.hash(query, options);
   }

   @Override
   public String toString() {
      return new ToStringBuilder(this).append("query", query).append("options", options).toString();
   }

   public enum Option {
      CASE_INSENSITIVE
   }
}
