/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.core.stacktrace;

import static com.tinubu.commons.lang.validation.Validate.notEmpty;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.Optional.ofNullable;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.tinubu.logexplorer.core.stacktrace.StackTrace.ExceptionType;

@SuppressWarnings("squid:S2166")
public class StackTraceException {

   /**
    * Max length for exception message in debug contexts.
    */
   private static final int DEBUG_MESSAGE_MAX_LENGTH = 25;

   /**
    * Exception type.
    */
   private Type type;

   /**
    * Optional exception message.
    */
   private String message;

   /**
    * Traced method history.
    */
   private List<TracedMethod> tracedMethods;

   /**
    * Optional cause exception.
    */
   private StackTraceException causedBy;

   /**
    * Optional wrapping exception.
    */
   private StackTraceException wrappedBy;

   public StackTraceException(Type type, String message, List<TracedMethod> tracedMethods) {
      this.type = notNull(type, "type");
      this.message = message;
      this.tracedMethods = notEmpty(tracedMethods, "tracedMethods");
   }

   public Type type() {
      return type;
   }

   // FIXME return Optional ?
   public String message() {
      return message;
   }

   public List<TracedMethod> tracedMethods() {
      return tracedMethods;
   }

   public Optional<StackTraceException> causedBy() {
      return ofNullable(causedBy);
   }

   public StackTraceException causedBy(StackTraceException exception) {
      this.causedBy = exception;
      return this;
   }

   public Optional<StackTraceException> wrappedBy() {
      return ofNullable(wrappedBy);
   }

   public StackTraceException wrappedBy(StackTraceException exception) {
      this.wrappedBy = exception;
      return this;
   }

   public void visit(StackTraceExceptionVisitor visitor, ExceptionType exceptionType) {
      visitor.visit(this, exceptionType);
   }

   protected String crop(String s, int maxLength) {
      if (s.length() > maxLength) {
         return message.substring(0, DEBUG_MESSAGE_MAX_LENGTH).trim() + "...";
      } else {
         return s;
      }
   }

   /**
    * @implNote causedBy and wrappedBy are removed from equality check to avoid infinite recursions.
    */
   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      StackTraceException that = (StackTraceException) o;
      return type.equals(that.type)
             && message.equals(that.message)
             && tracedMethods.equals(that.tracedMethods);
   }

   @Override
   public int hashCode() {
      return Objects.hash(type, message, tracedMethods);
   }

   /**
    * @implNote causedBy and wrappedBy are removed from hash computation to avoid infinite recursions.
    */
   @Override
   public String toString() {
      return new ToStringBuilder(this)
            .append("type", type)
            .append("message", crop(message, DEBUG_MESSAGE_MAX_LENGTH))
            .append("tracedMethods", tracedMethods)
            .append("causedBy", causedBy)
            .append("wrappedBy", wrappedBy)
            .toString();
   }

}
