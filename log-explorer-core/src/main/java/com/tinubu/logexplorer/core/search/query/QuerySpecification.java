/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.core.search.query;

import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Complete query set of information.
 *
 * @implSpec Immutable class implementation.
 */
public class QuerySpecification {

   private final List<Query> querySet;
   private final List<Query> notQuerySet;

   private QuerySpecification(List<Query> querySet, List<Query> notQuerySet) {
      this.querySet = notNull(querySet, "query");
      this.notQuerySet = notNull(notQuerySet, "notQuery");
   }

   public static QuerySpecification empty() {
      return new QuerySpecification(emptyList(), emptyList());
   }

   public static QuerySpecification of(List<Query> querySet, List<Query> notQuerySet) {
      return new QuerySpecification(querySet, notQuerySet);
   }

   public QuerySpecification withQuerySet(List<Query> querySet) {
      return new QuerySpecification(querySet, notQuerySet);
   }

   public QuerySpecification withNotQuerySet(List<Query> notQuerySet) {
      return new QuerySpecification(querySet, notQuerySet);
   }

   public QuerySpecification addQuery(Query query) {
      return new QuerySpecification(Stream.concat(querySet().stream(), Stream.of(query)).collect(toList()),
                                    notQuerySet());
   }

   public QuerySpecification addNotQuery(Query notQuery) {
      return new QuerySpecification(querySet(),
                                    Stream
                                          .concat(notQuerySet().stream(), Stream.of(notQuery))
                                          .collect(toList()));
   }

   public QuerySpecification filterQuerySet(Predicate<Query> filter) {
      return new QuerySpecification(querySet().stream().filter(filter).collect(toList()), notQuerySet());
   }

   public QuerySpecification filterNotQuerySet(Predicate<Query> filter) {
      return new QuerySpecification(querySet(), notQuerySet().stream().filter(filter).collect(toList()));
   }

   public QuerySpecification filterQuerySets(Predicate<Query> filter) {
      return filterQuerySet(filter).filterNotQuerySet(filter);
   }

   public QuerySpecification mapQuerySet(Function<Query, Query> mapper) {
      return new QuerySpecification(querySet().stream().map(mapper).collect(toList()), notQuerySet());
   }

   public QuerySpecification mapNotQuerySet(Function<Query, Query> mapper) {
      return new QuerySpecification(querySet(), notQuerySet().stream().map(mapper).collect(toList()));
   }

   public QuerySpecification mapQuerySets(Function<Query, Query> mapper) {
      return mapQuerySet(mapper).mapNotQuerySet(mapper);
   }

   /**
    * Set of queries that must all match.
    */
   public List<Query> querySet() {
      return querySet;
   }

   public <T extends Query> List<T> querySet(Class<T> queryClass) {
      return querySetByType(querySet, queryClass);
   }

   /**
    * Set of queries that must not match.
    */
   public List<Query> notQuerySet() {
      return notQuerySet;
   }

   public <T extends Query> List<T> notQuerySet(Class<T> queryClass) {
      return querySetByType(notQuerySet, queryClass);
   }

   private <T extends Query> List<T> querySetByType(List<Query> querySet, Class<T> queryClass) {
      return querySet
            .stream()
            .filter(query -> queryClass.isAssignableFrom(query.getClass()))
            .map(queryClass::cast)
            .collect(toList());
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      QuerySpecification querySpecification = (QuerySpecification) o;
      return Objects.equals(querySet, querySpecification.querySet) && Objects.equals(notQuerySet,
                                                                                     querySpecification.notQuerySet);
   }

   @Override
   public int hashCode() {
      return Objects.hash(querySet, notQuerySet);
   }

   @Override
   public String toString() {
      return new ToStringBuilder(this).append("query", querySet).append("notQuery", notQuerySet).toString();
   }

}
