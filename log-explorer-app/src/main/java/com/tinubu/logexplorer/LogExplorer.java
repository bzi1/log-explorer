/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer;

import static java.lang.System.exit;
import static java.util.stream.Collectors.toList;
import static org.fusesource.jansi.internal.CLibrary.isatty;
import static org.jline.reader.LineReader.Option.AUTO_FRESH_LINE;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.nio.file.Paths;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.fusesource.jansi.AnsiConsole;
import org.jline.reader.EndOfFileException;
import org.jline.reader.LineReader;
import org.jline.reader.LineReaderBuilder;
import org.jline.reader.UserInterruptException;
import org.jline.reader.impl.history.DefaultHistory;
import org.jline.terminal.Terminal;
import org.jline.terminal.Terminal.SignalHandler;
import org.jline.terminal.TerminalBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tinubu.logexplorer.cli.console.ConsoleCommand;
import com.tinubu.logexplorer.cli.console.ConsoleContext;
import com.tinubu.logexplorer.cli.console.commands.AliasCommand;
import com.tinubu.logexplorer.cli.console.commands.BackendCommand;
import com.tinubu.logexplorer.cli.console.commands.ContextCommand;
import com.tinubu.logexplorer.cli.console.commands.DebugCommand;
import com.tinubu.logexplorer.cli.console.commands.ExitCommand;
import com.tinubu.logexplorer.cli.console.commands.FollowCommand;
import com.tinubu.logexplorer.cli.console.commands.FormatterCommand;
import com.tinubu.logexplorer.cli.console.commands.FromDateCommand;
import com.tinubu.logexplorer.cli.console.commands.HelpCommand;
import com.tinubu.logexplorer.cli.console.commands.LinesCommand;
import com.tinubu.logexplorer.cli.console.commands.OutputCommand;
import com.tinubu.logexplorer.cli.console.commands.ProfileCommand;
import com.tinubu.logexplorer.cli.console.commands.QueryCommand;
import com.tinubu.logexplorer.cli.console.commands.ResetCommand;
import com.tinubu.logexplorer.cli.console.commands.SetCommand;
import com.tinubu.logexplorer.cli.console.commands.StatusCommand;
import com.tinubu.logexplorer.cli.console.commands.TimezoneCommand;
import com.tinubu.logexplorer.cli.console.commands.ToDateCommand;
import com.tinubu.logexplorer.cli.console.commands.UnaliasCommand;
import com.tinubu.logexplorer.cli.console.commands.UnprofileCommand;
import com.tinubu.logexplorer.core.backend.BackendInterruptionState;
import com.tinubu.logexplorer.core.config.Configuration.AnsiFlag;
import com.tinubu.logexplorer.core.config.Configuration.ProgressMeterFlag;
import com.tinubu.logexplorer.core.config.DirectConfiguration;
import com.tinubu.logexplorer.core.config.MapConfiguration;
import com.tinubu.logexplorer.core.search.QueryDateRange;
import com.tinubu.logexplorer.core.search.query.Query;
import com.tinubu.logexplorer.core.search.query.QueryBuilder;
import com.tinubu.logexplorer.core.search.query.QuerySpecification;

import picocli.CommandLine;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

@CommandLine.Command(name = "log-explorer", description = "Universal log explorer", version = {
      "log-explorer ${git.build.version:--} (${git.build.time:--})",
      "Git: ${git.commit.id.abbrev:--} (${git.commit.time:--})",
      "JVM: ${java.version} (${java.vendor} ${java.vm.name} ${java.vm.version})" },
                     mixinStandardHelpOptions = true, sortOptions = false, parameterListHeading = "\n",
                     optionListHeading = "\n")
@SuppressWarnings("squid:S106")
public class LogExplorer implements Runnable {

   private static final Logger logger = LoggerFactory.getLogger(LogExplorer.class);

   private static final File HISTORY_FILE =
         Paths.get(System.getProperty("user.home"), ".log-explorer.history").toFile();
   private static final Integer HISTORY_FILE_SIZE = 500000;
   private static final Integer HISTORY_SIZE = 500;
   private static final int USAGE_HELP_WIDTH = 120;

   static {
      ToStringBuilder.setDefaultStyle(ToStringStyle.SHORT_PREFIX_STYLE);
   }

   @Parameters(index = "*", description = "Query string.", defaultValue = "", paramLabel = "<query>")
   private List<String> queryParts;

   @Option(names = { "-f", "--from-date" }, arity = "1",
           description = "Query from date (inclusive) or duration from now [${DEFAULT-VALUE}].",
           defaultValue = "PT1H", paramLabel = "<date>")
   private String fromDate;

   @Option(names = { "-t", "--to-date" }, arity = "1", description = "Query to date (inclusive) [now].",
           paramLabel = "<date>", defaultValue = "now")
   private String toDate;

   @Option(names = { "-p", "--profiles" },
           description = "Configuration profiles by ascending priority order.", paramLabel = "<profile>")
   @SuppressWarnings("FieldMayBeFinal")
   private LinkedHashSet<String> profiles = new LinkedHashSet<>();

   @Option(names = { "-c", "--config" }, arity = "1", description = "Configuration file.",
           paramLabel = "<file>")
   private File configFile;

   @Option(names = { "-o", "--output" }, arity = "1", description = "Output file.", paramLabel = "<file>")
   private File outputFile;

   @Option(names = { "-a", "--append" }, description = "Append to output file.")
   @SuppressWarnings("FieldMayBeFinal")
   private boolean appendOutputFile = false;

   @Option(names = { "-D" }, description = "Configuration key/value.", paramLabel = "<key>=<value>")
   @SuppressWarnings("FieldMayBeFinal")
   private Map<String, String> cliConfig = new HashMap<>();

   @Option(names = { "-d", "--debug" }, description = "Debug mode.")
   @SuppressWarnings("FieldMayBeFinal")
   private boolean debugMode = false;

   @Option(names = { "--ansi" }, arity = "1", description = "ANSI colors.",
           paramLabel = "<auto|never|always>")
   private AnsiFlag ansiFlag;

   @Option(names = { "--progress" }, arity = "1", description = "Progress meter.",
           paramLabel = "<auto|never|always>")
   private ProgressMeterFlag progressMeterFlag;

   @Option(names = { "-i", "--interactive" }, description = "Interactive console mode.")
   @SuppressWarnings("FieldMayBeFinal")
   private boolean consoleMode = false;

   @Option(names = { "-n" }, arity = "1", description = "Number of latest lines to return.")
   private Long numberLines;

   @Option(names = { "-F", "--follow" }, description = "Follow mode.")
   @SuppressWarnings("FieldMayBeFinal")
   private boolean followMode = false;

   @Option(names = { "-z", "--timezone" }, description = "Timezone.")
   @SuppressWarnings("FieldMayBeFinal")
   private ZoneId timezone;

   public static void main(String[] args) {
      loadGitProperties();

      exit(new LogExplorer().parse(args));
   }

   /** Entry point. */
   public int parse(String[] args) {
      CommandLine commandLine = new CommandLine(this);
      commandLine.setToggleBooleanFlags(false);
      commandLine.setCaseInsensitiveEnumValuesAllowed(true);
      commandLine.setUsageHelpWidth(USAGE_HELP_WIDTH);

      return commandLine.execute(args);
   }

   /**
    * Main loop.
    */
   @Override
   public void run() {
      try {
         if (followMode && !"now".equals(toDate)) {
            throw new IllegalArgumentException("toDate must be 'now' in follow mode");
         }
         if (consoleMode && !ttyTerminal()) {
            throw new IllegalArgumentException("Interactive mode can't be used in non-tty consoles");
         }

         try (Context context = Context.create(fromDate,
                                               toDate,
                                               outputFile,
                                               appendOutputFile,
                                               profiles,
                                               followMode,
                                               consoleMode,
                                               configFile,
                                               new MapConfiguration(cliConfig),
                                               new DirectConfiguration()
                                                     .withDebug(debugMode)
                                                     .withAnsi(ansiFlag)
                                                     .withProgress(progressMeterFlag)
                                                     .withNumberLines(numberLines)
                                                     .withTimezone(timezone))) {

            configureAnsiConsole(context.configuration().ansi());

            mainLoop(context);
         }
      } catch (Exception e) {
         generalError(e);
      }
   }

   /**
    * Searches and loads git.properties file into system properties.
    */
   private static void loadGitProperties() {
      try (InputStream gitPropertiesStream = Thread
            .currentThread()
            .getContextClassLoader()
            .getResourceAsStream("git.properties")) {
         if (gitPropertiesStream != null) {
            Properties gitProperties = new Properties();
            gitProperties.load(gitPropertiesStream);

            gitProperties.forEach((k, v) -> System.setProperty(k.toString(), v.toString()));
         }
      } catch (IOException e) {
         throw new IllegalStateException(e);
      }
   }

   private void mainLoop(Context context) throws IOException {
      if (consoleMode) {
         interactiveConsole(context);
      } else {
         QuerySpecification querySpecification = QuerySpecification
               .empty()
               .withQuerySet(context.configuration().query().flatten())
               .withNotQuerySet(context.configuration().notQuery().flatten());

         querySpecification = cliQuery().map(querySpecification::addQuery).orElse(querySpecification);

         context
               .search()
               .search(querySpecification,
                       new QueryDateRange(context.fromZonedDateTime(), context.toZonedDateTime()),
                       context.configuration().numberLines(),
                       context.followMode());
      }
   }

   /** Generates CLI query from argument parts. */
   private Optional<Query> cliQuery() {
      String queryString = String.join(" ", queryParts).trim();

      if (!queryString.isEmpty()) {
         return Optional.of(QueryBuilder.parseQuery(queryString));
      } else {
         return Optional.empty();
      }
   }

   /**
    * Manages uncaught exception in non interactive mode.
    *
    * @param e uncaught exception
    */
   private void generalError(Exception e) {
      System.err.println("[E] " + e.getMessage());
      if (logger.isDebugEnabled()) {
         logger.debug(e.getMessage(), e);
      }
      exit(1);
   }

   /**
    * Configures {@link AnsiConsole} depending on resolved {@link AnsiFlag} mode.
    * <p>
    * This global {@link org.fusesource.jansi.Ansi} configuration is dedicated to console exporters.
    *
    * @implSpec {@link org.fusesource.jansi.Ansi#setEnabled(boolean)} is disabled to strip ANSI codes
    *       from {@link org.fusesource.jansi.Ansi::ansi()} commands when console is not ANSI-enabled.
    * @implSpec {@code jansi.force=true} property is used to always enable the output console to
    *       support ANSI codes even when console is not a tty.
    * @implSpec in auto mode, if stdout is not a tty, ANSI codes are disabled even if progress meter
    *       is enabled (it outputs to stderr but priority is given to performance).
    */
   private void configureAnsiConsole(AnsiFlag ansi) {
      switch (ansi) {
         case AUTO:
            if (ttyTerminal()) {
               logger.debug("ANSI auto mode : Setup ANSI console for tty console");
               if (ideaTerminal()) {
                  System.setProperty("jansi.force", "true");
               }
               AnsiConsole.systemInstall();
               Runtime.getRuntime().addShutdownHook(new Thread(AnsiConsole::systemUninstall));
            } else {
               logger.debug("ANSI auto mode : Disable ANSI code generation for non tty console");
               org.fusesource.jansi.Ansi.setEnabled(false);
            }
            break;
         case NEVER:
            logger.debug("ANSI never mode : Disable ANSI code generation");
            org.fusesource.jansi.Ansi.setEnabled(false);
            break;
         case ALWAYS:
            logger.debug("ANSI always mode : Setup ANSI console (force)");
            System.setProperty("jansi.force", "true");
            AnsiConsole.systemInstall();
            Runtime.getRuntime().addShutdownHook(new Thread(AnsiConsole::systemUninstall));
            break;
      }
   }

   /**
    * Returns true if running in a TTY terminal.
    *
    * @return true if running in a TTY terminal.
    */
   private boolean ttyTerminal() {
      return isatty(1) != 0 || ideaTerminal();
   }

   /**
    * Detects if IntelliJ IDEA terminal is used.
    *
    * @return true if running application in IDEA terminal.
    */
   private boolean ideaTerminal() {
      try {
         Class<?> phClass = Class.forName("java.lang.ProcessHandle");
         Object current = phClass.getMethod("current").invoke(null);
         Object parent = ((Optional<?>) phClass.getMethod("parent").invoke(current)).orElse(null);
         Method infoMethod = phClass.getMethod("info");
         Object info = infoMethod.invoke(parent);
         Object command =
               ((Optional<?>) infoMethod.getReturnType().getMethod("command").invoke(info)).orElse(null);
         return command != null && ((String) command).contains("idea");
      } catch (Exception t) {
         return false;
      }
   }

   /**
    * @implNote {@link LineReader.Option#AUTO_FRESH_LINE} is required to cleanup progress meter when
    *       it is enabled.
    */
   private void interactiveConsole(Context context) throws IOException {
      Terminal terminal = TerminalBuilder
            .builder()
            .jna(false)
            .jansi(true)
            .signalHandler(interactiveConsoleSignalHandler())
            .build();
      ConsoleCommand helpCommand = new HelpCommand();
      List<ConsoleCommand> commands = Arrays.asList(helpCommand,
                                                    new StatusCommand(),
                                                    new SetCommand(),
                                                    new TimezoneCommand(),
                                                    new FromDateCommand(),
                                                    new ToDateCommand(),
                                                    new QueryCommand(),
                                                    new ProfileCommand(),
                                                    new UnprofileCommand(),
                                                    new ResetCommand(),
                                                    new LinesCommand(),
                                                    new FollowCommand(),
                                                    new OutputCommand(),
                                                    new ContextCommand(),
                                                    new DebugCommand(),
                                                    new AliasCommand(),
                                                    new UnaliasCommand(),
                                                    new BackendCommand(),
                                                    new FormatterCommand(),
                                                    new ExitCommand());
      try (ConsoleContext consoleContext = new ConsoleContext(context, terminal, commands)) {
         LineReader reader = lineReader(terminal, consoleContext);
         Optional<Query> initialQuery = cliQuery();

         try {
            while (!consoleContext.exitConsole()) {
               String line;
               try {
                  line = reader.readLine(consoleContext.prompt(),
                                         null,
                                         initialQuery.map(Query::queryString).orElse(null));

                  if (!line.startsWith("#")) {
                     if (line.isBlank()) {
                        helpCommand.execute(consoleContext);
                     } else {
                        List<String> lineElements = Arrays.asList(line.split("\\s+"));

                        checkAmbiguousCommand(consoleContext, lineElements.get(0));

                        Optional<ConsoleCommand> currentCommand = consoleContext
                              .commands()
                              .stream()
                              .filter(command -> inputCommand(lineElements,
                                                              command.commandName(),
                                                              command.minimumParameters()).isPresent())
                              .findFirst();

                        currentCommand.ifPresentOrElse(command -> command.execute(consoleContext,
                                                                                  lineElements), () -> {
                           consoleContext.printError("Bad command syntax");
                           helpCommand.execute(consoleContext);
                        });
                     }
                  }
                  if (consoleContext.commandsCompleterUpdated()) {
                     reader = lineReader(terminal, consoleContext);
                     consoleContext.commandsCompleterUpdated(false);
                  }
               } catch (UserInterruptException e) {
                  /* Ignored. */
               } catch (EndOfFileException e) {
                  consoleContext.exitConsole(true);
               } catch (Exception e) {
                  consoleContext.printError(e);
               } finally {
                  BackendInterruptionState.interrupting(false);
                  initialQuery = Optional.empty();
               }
            }
         } finally {
            reader.getHistory().save();
         }
      }
   }

   private LineReader lineReader(Terminal terminal, ConsoleContext consoleContext) {
      return LineReaderBuilder
            .builder()
            .completer(consoleContext.commandsCompleter())
            .history(new DefaultHistory())
            .variable(LineReader.HISTORY_FILE, HISTORY_FILE.toString())
            .variable(LineReader.HISTORY_SIZE, HISTORY_SIZE)
            .variable(LineReader.HISTORY_FILE_SIZE, HISTORY_FILE_SIZE)
            .option(AUTO_FRESH_LINE, true)
            .terminal(terminal)
            .build();
   }

   /** Intercept ^C to interrupt backend execution. */
   private SignalHandler interactiveConsoleSignalHandler() {
      return signal -> BackendInterruptionState.interrupting(true);
   }

   private void checkAmbiguousCommand(ConsoleContext context, String command) {
      List<String> ambiguousCommands = context
            .commands()
            .stream()
            .map(ConsoleCommand::commandName)
            .filter(commandName -> commandName.toLowerCase().startsWith(command.toLowerCase()))
            .collect(toList());

      if (ambiguousCommands.size() > 1) {
         context.error("Ambiguous commands : %s", String.join(",", ambiguousCommands));
      }
   }

   /**
    * Returns the complete matched command if the input line first element matches any command.
    *
    * @param lineElements parsed line elements
    * @param command target command to match
    * @param minimumParams minimum required number of parameters to command
    */
   private Optional<String> inputCommand(List<String> lineElements, String command, int minimumParams) {
      if (lineElements.size() <= minimumParams) {
         return Optional.empty();
      }

      return Optional.of(command).filter(c -> c.toLowerCase().startsWith(lineElements.get(0).toLowerCase()));
   }

}
