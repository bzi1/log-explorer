/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.cli.console.commands;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Optional;

import org.jline.reader.Completer;
import org.jline.reader.impl.completer.ArgumentCompleter;

import com.tinubu.logexplorer.cli.console.ConsoleCommand;
import com.tinubu.logexplorer.cli.console.ConsoleContext;
import com.tinubu.logexplorer.cli.console.commands.AliasCommand.AliasTargetCommand;

public class UnaliasCommand extends ConsoleCommand {

   @Override
   public String commandName() {
      return "unalias";
   }

   @Override
   public CommandHelpDescription helpDescription() {
      return new CommandHelpDescription("delete an alias", "<alias>");
   }

   @Override
   public int minimumParameters() {
      return 1;
   }

   @Override
   public void execute(ConsoleContext context, List<String> options) {
      String aliasName = options.get(1);
      Optional<ConsoleCommand> aliasCommand = context
            .commands()
            .stream()
            .filter(command -> command instanceof AliasTargetCommand)
            .filter(command -> command.commandName().equals(aliasName))
            .findFirst();

      aliasCommand.ifPresentOrElse(context::removeCommand,
                                   () -> context.error("Unknown '%s' alias", aliasName));
   }

   @Override
   public Completer completer(ConsoleContext context) {
      return new ArgumentCompleter(new CommandCompleter(),
                                   new LazyStringsCompleter(() -> context
                                         .commands()
                                         .stream()
                                         .filter(command -> command instanceof AliasTargetCommand)
                                         .map(ConsoleCommand::commandName)
                                         .collect(toList())));
   }
}