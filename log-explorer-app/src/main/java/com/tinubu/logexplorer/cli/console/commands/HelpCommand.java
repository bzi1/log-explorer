/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.cli.console.commands;

import static org.fusesource.jansi.Ansi.ansi;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.fusesource.jansi.Ansi;
import org.jline.builtins.Completers.OptDesc;
import org.jline.builtins.Completers.OptionCompleter;
import org.jline.reader.Completer;
import org.jline.reader.impl.completer.ArgumentCompleter;
import org.jline.reader.impl.completer.NullCompleter;
import org.jline.reader.impl.completer.StringsCompleter;

import com.tinubu.logexplorer.cli.console.ConsoleCommand;
import com.tinubu.logexplorer.cli.console.ConsoleContext;
import com.tinubu.logexplorer.cli.console.commands.AliasCommand.AliasTargetCommand;

public class HelpCommand extends ConsoleCommand {

   @Override
   public String commandName() {
      return "help";
   }

   @Override
   public CommandHelpDescription helpDescription() {
      return new CommandHelpDescription("this help or optional command help", "[command]");
   }

   @Override
   public void execute(ConsoleContext context, List<String> options) {

      String commandHelp = options.size() > 1 ? options.get(1) : null;

      if (commandHelp == null) {
         helpCommands(context);
      } else {
         helpCommand(context, commandHelp);
      }
   }

   private void helpCommand(ConsoleContext context, String commandName) {
      Optional<ConsoleCommand> command =
            context.commands().stream().filter(c -> c.commandName().equals(commandName)).findFirst();

      command.ifPresentOrElse(c -> {
         context.println();
         context.println("   Command :");
         context.println();
         commandHelpSyntax(context, c);
         context.println();
         commandHelpVerbose(context, c);
      }, () -> context.error("Unknown '%s' command", commandName));
   }

   private void helpCommands(ConsoleContext context) {
      context.println();
      context.println("   Commands :");
      context.println();
      context.commands().forEach(command -> commandHelpSyntax(context, command));
   }

   private void commandHelpSyntax(ConsoleContext context, ConsoleCommand command) {
      Ansi commandHelp = ansi().a("      ");
      if (command instanceof AliasTargetCommand) {
         commandHelp.fgBrightGreen();
      } else {
         commandHelp.fgGreen();
      }
      commandHelp.a(command.commandName()).reset();
      command.helpDescription().options().forEach(option -> commandHelp.a(" ").fgYellow().a(option).reset());
      commandHelp.a(" : ").fgBlue().a(command.helpDescription().description()).reset();
      context.println(commandHelp);
   }

   private void commandHelpVerbose(ConsoleContext context, ConsoleCommand command) {
      command.helpDescription().verboseDescription().ifPresent(description -> {
         Stream.of(description.split("\n")).forEach(descriptionLine -> {
            context.println(ansi().a("      ") + descriptionLine);
         });
      });
   }

   @Override
   public Completer completer(ConsoleContext context) {
      return new ArgumentCompleter(new CommandCompleter(),
                                   new OptionCompleter(Arrays.asList(new StringsCompleter("-v"),
                                                                     NullCompleter.INSTANCE),
                                                       (List<OptDesc>) null,
                                                       1),
                                   NullCompleter.INSTANCE);
   }

}

