/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.cli.console;

import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.Collections.singletonMap;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toList;
import static org.fusesource.jansi.Ansi.ansi;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jline.reader.Completer;
import org.jline.reader.impl.completer.AggregateCompleter;
import org.jline.terminal.Terminal;

import com.tinubu.logexplorer.Context;
import com.tinubu.logexplorer.cli.console.commands.AliasCommand.AliasTargetCommand;
import com.tinubu.logexplorer.core.config.Configuration.Aliases;
import com.tinubu.logexplorer.core.lang.TypeConverter;

public class ConsoleContext extends Context {
   private String prompt;
   private final Terminal terminal;
   private boolean contextMode = true;
   private final List<ConsoleCommand> commands;

   /** Flag to exit console mode. */
   private boolean exitConsole = false;
   private boolean commandsCompleterUpdated = false;
   private Completer commandsCompleter;

   public ConsoleContext(Context context, Terminal terminal, List<ConsoleCommand> commands) {
      super(context);

      this.terminal = notNull(terminal, "terminal");
      this.commands = new ArrayList<>(notNull(commands, "commands"));
      this.commands.addAll(aliasTargetCommands(context.configuration().aliases()));
      this.commandsCompleter = createCommandsCompleter(this.commands);
      updatePrompt();
   }

   private List<AliasTargetCommand> aliasTargetCommands(Aliases aliases) {
      return notNull(aliases, "aliases")
            .entrySet()
            .stream()
            .map(e -> createAliasTargetCommand(e.getKey(), e.getValue()))
            .collect(toList());
   }

   @Override
   public void fromDate(String fromDate) {
      super.fromDate(fromDate);
      updatePrompt();
   }

   @Override
   public void toDate(String toDate) {
      super.toDate(toDate);
      updatePrompt();
   }

   @Override
   public void followMode(boolean followMode) {
      super.followMode(followMode);
      updatePrompt();
   }

   @Override
   public void numberLines(Long numberLines) {
      super.numberLines(numberLines);
      updatePrompt();
   }

   @Override
   public void timezone(ZoneId timezone) {
      super.timezone(timezone);
      updatePrompt();
   }

   @Override
   public void resetProfiles(LinkedHashSet<String> profiles) {
      super.resetProfiles(profiles);
      updatePrompt();
   }

   @Override
   public void addProfile(String profile) {
      super.addProfile(profile);
      updateAliases();
      updatePrompt();
   }

   @Override
   public void removeProfile(String profile) {
      super.removeProfile(profile);
      updateAliases();
      updatePrompt();
   }

   @Override
   public void resetQueries() {
      super.resetQueries();
      updatePrompt();
   }

   @Override
   public void resetParameters() {
      super.resetParameters();
      updatePrompt();
   }

   @Override
   public void updateBackend(String name) {
      super.updateBackend(name);
      updatePrompt();
   }

   private void updatePrompt() {
      prompt(interactivePrompt());
   }

   public String prompt() {
      return prompt;
   }

   public void prompt(String prompt) {
      this.prompt = prompt;
   }

   public Terminal terminal() {
      return terminal;
   }

   public boolean contextMode() {
      return contextMode;
   }

   public void contextMode(boolean contextMode) {
      this.contextMode = contextMode;
      updatePrompt();
   }

   public List<ConsoleCommand> commands() {
      return commands;
   }

   private Completer createCommandsCompleter(List<ConsoleCommand> commands) {
      return new AggregateCompleter(new ArrayList<>(commands
                                                          .stream()
                                                          .map(command -> command.completer(this))
                                                          .collect(toList())));
   }

   public Completer commandsCompleter() {
      return commandsCompleter;
   }

   public void addCommand(ConsoleCommand command) {
      this.commands.add(command);
      this.commandsCompleter = createCommandsCompleter(commands);
      this.commandsCompleterUpdated = true;
   }

   public void removeCommand(ConsoleCommand command) {
      this.commands.remove(command);
      this.commandsCompleter = createCommandsCompleter(commands);
      this.commandsCompleterUpdated = true;
   }

   /** Alias reloading after configuration updates. */
   public void updateAliases() {
      configuration().aliases().forEach((alias, target) -> {
         commands()
               .stream()
               .filter(command -> command.commandName().equals(alias))
               .findAny()
               .ifPresent(this::removeCommand);
         addCommand(createAliasTargetCommand(alias, target));
      });
   }

   public AliasTargetCommand createAliasTargetCommand(String alias, List<String> target) {
      notNull(alias, "alias");
      notNull(target, "target");

      if (target.size() < 1) {
         throw new IllegalStateException(String.format("Can't create alias '%s' : no target", alias));
      }

      Optional<ConsoleCommand> existingCommand =
            commands().stream().filter(command -> command.commandName().equals(alias)).findAny();

      if (existingCommand.isPresent()) {
         throw new IllegalArgumentException(String.format(
               "Can't create alias '%s' : command '%s' already exist",
               alias,
               alias));
      }

      String targetCommandName = target.get(0);

      ConsoleCommand targetCommand = commands()
            .stream()
            .filter(command -> command.commandName().equals(targetCommandName))
            .findFirst()
            .orElseThrow(() -> new IllegalArgumentException(String.format(
                  "Can't create alias '%s' : Unknown '%s' target command",
                  alias,
                  targetCommandName)));

      return new AliasTargetCommand(alias, target.stream().skip(1).collect(toList()), targetCommand);
   }

   public AliasTargetCommand createAliasTargetCommand(String alias, String target) {
      notNull(alias, "alias");
      notNull(target, "target");

      String[] targetComponents = target.split(" ");

      return createAliasTargetCommand(alias, Arrays.asList(targetComponents));
   }

   public boolean commandsCompleterUpdated() {
      return commandsCompleterUpdated;
   }

   public ConsoleContext commandsCompleterUpdated(boolean commandsCompleterUpdated) {
      this.commandsCompleterUpdated = commandsCompleterUpdated;
      return this;
   }

   public boolean exitConsole() {
      return exitConsole;
   }

   public void exitConsole(boolean exitConsole) {
      this.exitConsole = exitConsole;
   }

   /**
    * Display uncaught exception in console.
    *
    * @param e uncaught exception
    */
   public void printError(Exception e) {
      if (e instanceof ConsoleException) {
         printError(e.getMessage());
      } else {
         printError(e.getClass().getSimpleName() + (e.getMessage() != null ? (": " + e.getMessage()) : ""));
      }
      if (configuration().debug()) {
         ExceptionUtils.printRootCauseStackTrace(e, terminal().writer());
      }
   }

   /**
    * Display error message in console.
    *
    * @param error error message
    * @param args optional argument for {@link String#format(String, Object...)}.
    */
   public void printError(String error, Object... args) {
      println(ansi()
                    .a("[")
                    .fgBrightRed()
                    .a("E")
                    .reset()
                    .a("] ")
                    .fgRed()
                    .a(String.format(error, args))
                    .reset());
   }

   public void println(String message, Object... args) {
      terminal.writer().println(String.format(message, args));
   }

   public void println() {
      terminal.writer().println();
   }

   public void println(org.fusesource.jansi.Ansi message, Object... args) {
      println(String.format(message.toString(), args));
   }

   public void print(String message, Object... args) {
      terminal.writer().print(String.format(message, args));
   }

   public void print(org.fusesource.jansi.Ansi message, Object... args) {
      print(String.format(message.toString(), args));
   }

   @SuppressWarnings("unchecked")
   public void printObject(Object value, String indent) {
      if (value instanceof Map) {
         ((Map<Object, Object>) value).forEach((k, v) -> {
            println();
            print(ansi().a(indent).fgYellow().a(TypeConverter.stringValue(k)).reset().a(": "));
            printObject(v, indent + "   ");
         });
      } else if (value instanceof Collection) {
         ((Collection<Object>) value).forEach(v -> {
            println();
            print(ansi().a(indent + "- "));
            printObject(v, indent + "  " + "    ");
         });
      } else {
         print(ansi().fgBlue().a(TypeConverter.stringValue(value)).reset());
      }
   }

   public void printObject(Object value) {
      printObject(value, "");
   }

   /**
    * Generates the interactive prompt.
    *
    * @return prompt string
    */
   private String interactivePrompt() {
      org.fusesource.jansi.Ansi prompt = ansi();

      if (contextMode) {
         Map<String, Object> dateRanges = new LinkedHashMap<>();
         dateRanges.put("timezone", configuration().timezone());
         dateRanges.put("fromDate", fromDate());
         dateRanges.put("toDate", toDate());
         dateRanges.put("lines",
                        configuration().numberLines() != null && configuration().numberLines() >= 0
                        ? configuration().numberLines()
                        : "unlimited");
         prompt.a(System.lineSeparator()).a(interactivePromptContext("range", dateRanges));
         if (!profiles().isEmpty()) {
            prompt.a(interactivePromptContext("profiles", singletonMap("", String.join(" > ", profiles()))));
         }

         Set<String> queryGroups = new HashSet<>();
         queryGroups.addAll(configuration().query().keySet());
         queryGroups.addAll(configuration().notQuery().keySet());

         for (String queryGroup : queryGroups) {
            ofNullable(configuration()
                             .query()
                             .get(queryGroup)).ifPresent(group -> group.forEach(query -> prompt.a(
                  interactivePromptContext("{" + queryGroup + "} query",
                                           singletonMap("query", query.queryString())))));
            ofNullable(configuration()
                             .notQuery()
                             .get(queryGroup)).ifPresent(group -> group.forEach(query -> prompt.a(
                  interactivePromptContext("{" + queryGroup + "} query",
                                           singletonMap("notQuery", query.queryString())))));
         }
      }

      if (followMode()) {
         prompt.fgBrightCyan().a("follow").reset().a(" ");
      }

      return prompt.fgCyan().a(configuration().backend()).reset().a("> ").toString();
   }

   private org.fusesource.jansi.Ansi interactivePromptContext(String context, Map<String, Object> values) {
      notNull(context);
      notNull(values);

      org.fusesource.jansi.Ansi contextAnsi = ansi().a("[").fgGreen().a(context).reset().a("]");

      values.forEach((prefix, value) -> {
         contextAnsi.a(" ");
         if (!prefix.isBlank()) {
            contextAnsi.fgYellow().a(prefix).reset().a(" : ");
         }
         contextAnsi.fgBlue().a(value).reset();
      });

      contextAnsi.a(System.lineSeparator());

      return contextAnsi;
   }

   /**
    * Throw error from console.
    *
    * @param message error message
    * @param args optional argument for {@link String#format(String, Object...)}.
    */
   public void error(String message, Object... args) {
      throw new ConsoleException(message, args);
   }

}
