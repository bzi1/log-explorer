/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.cli.console.commands;

import java.util.List;

import org.jline.reader.Completer;
import org.jline.reader.impl.completer.ArgumentCompleter;
import org.jline.reader.impl.completer.NullCompleter;
import org.jline.reader.impl.completer.StringsCompleter;

import com.tinubu.logexplorer.Context;
import com.tinubu.logexplorer.cli.console.ConsoleCommand;
import com.tinubu.logexplorer.cli.console.ConsoleContext;

public class UnprofileCommand extends ConsoleCommand {

   @Override
   public String commandName() {
      return "unprofile";
   }

   @Override
   public int minimumParameters() {
      return 1;
   }

   @Override
   public CommandHelpDescription helpDescription() {
      return new CommandHelpDescription("remove profile", "<profile>");
   }

   @Override
   public void execute(ConsoleContext context, List<String> options) {
      String profile = options.get(1);

      context.removeProfile(profile);
   }

   @Override
   public Completer completer(ConsoleContext context) {
      return new ArgumentCompleter(new CommandCompleter(),
                                   new UnprofileCompleter(context),
                                   NullCompleter.INSTANCE);
   }

   /** Completer matching known currently loaded profiles. */
   public static class UnprofileCompleter extends StringsCompleter {
      public UnprofileCompleter(Context context) {
         super(context.profiles());
      }
   }

}
