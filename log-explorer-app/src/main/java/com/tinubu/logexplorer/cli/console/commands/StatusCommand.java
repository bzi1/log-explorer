/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.cli.console.commands;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tinubu.logexplorer.cli.console.ConsoleCommand;
import com.tinubu.logexplorer.cli.console.ConsoleContext;

public class StatusCommand extends ConsoleCommand {

   @Override
   public String commandName() {
      return "status";
   }

   @Override
   public CommandHelpDescription helpDescription() {
      return new CommandHelpDescription("options status");
   }

   @Override
   public void execute(ConsoleContext context, List<String> lineElements) {
      context.println();
      context.println("   Status :");

      Map<String, Object> options = new HashMap<>();
      if (!context.contextMode()) {
         options.put("fromDate", context.fromDate());
         options.put("toDate", context.toDate());
         options.put("lines", context.configuration().numberLines());
         options.put("follow", context.followMode());
         options.put("profiles", context.profiles());
         options.put("query", context.configuration().query());
         options.put("notQuery", context.configuration().notQuery());
      }
      options.put("backendUris", context.configuration().backendUris());
      options.put("debug", context.configuration().debug());
      options.put("formatter", context.formatter().displayString());

      context.printObject(options, "      ");
      context.println();
   }
}
