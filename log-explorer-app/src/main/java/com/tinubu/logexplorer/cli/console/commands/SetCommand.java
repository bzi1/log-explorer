/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.cli.console.commands;

import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static org.fusesource.jansi.Ansi.ansi;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

import org.fusesource.jansi.Ansi;
import org.jline.reader.Candidate;
import org.jline.reader.Completer;
import org.jline.reader.LineReader;
import org.jline.reader.ParsedLine;
import org.jline.reader.impl.completer.ArgumentCompleter;
import org.jline.reader.impl.completer.NullCompleter;

import com.tinubu.logexplorer.cli.console.ConsoleCommand;
import com.tinubu.logexplorer.cli.console.ConsoleContext;
import com.tinubu.logexplorer.cli.console.commands.AliasCommand.AliasTargetCommand;
import com.tinubu.logexplorer.core.config.Configuration.Parameters;
import com.tinubu.logexplorer.core.extension.parameter.ParameterDescription;
import com.tinubu.logexplorer.core.lang.TypeConverter;

public class SetCommand extends ConsoleCommand {

   @Override
   public String commandName() {
      return "set";
   }

   @Override
   public CommandHelpDescription helpDescription() {
      return new CommandHelpDescription(
            "set parameter value or remove it. Display parameters without arguments",
            "[<parameter>",
            "[<value>]]");
   }

   @Override
   public void execute(ConsoleContext context, List<String> options) {
      String parameter = options.size() > 1 ? options.get(1) : null;
      String value = options.size() > 2 ? options.stream().skip(2).collect(joining(" ")) : null;

      if (parameter != null) {
         Parameters parameters = context.configuration().parameters();
         if (value != null) {
            parameters.put(parameter, value);
         } else {
            parameters.remove(parameter);
         }
         context.parameters(parameters);
      } else {
         context.println();
         context.println("   Parameters :");
         context.println();
         context
               .availableParameters()
               .stream()
               .sorted(Comparator.comparing(ParameterDescription::name))
               .forEach(p -> describeParameter(context, p));
      }
   }

   private void describeParameter(ConsoleContext context, ParameterDescription<?> parameter) {
      Ansi parameterDescription = ansi()
            .a("      ")
            .fgYellow()
            .a(parameter.name())
            .reset()
            .a(" (")
            .fgGreen()
            .a(parameter.type().getSimpleName())
            .reset()
            .a(")");

      if (parameter.description() != null) {
         parameterDescription.a(" : ").fgBlue().a(parameter.description()).reset();
      }

      Object parameterValue = context.configuration().parameters().get(parameter.name());
      if (parameterValue != null) {
         parameterDescription
               .a(" [")
               .fgBrightCyan()
               .a(TypeConverter.stringValue(parameterValue))
               .reset()
               .a("]");
      } else if (parameter.defaultValue() != null) {
         parameterDescription.a(" [").fgCyan().a(parameter.defaultValue()).reset().a("]");
      }

      context.println(parameterDescription);
   }

   @Override
   public Completer completer(ConsoleContext context) {
      return new ArgumentCompleter(new CommandCompleter(),
                                   new ParameterCompleter(context),
                                   new ParameterValueCompleter(context),
                                   NullCompleter.INSTANCE);
   }

   private static class ParameterCompleter extends LazyStringsCompleter {
      public ParameterCompleter(ConsoleContext context) {
         super(() -> notNull(context, "context")
               .availableParameters()
               .stream()
               .map(ParameterDescription::name)
               .collect(toList()));
      }
   }

   private static class ParameterValueCompleter implements Completer {
      private final ConsoleContext context;

      public ParameterValueCompleter(ConsoleContext context) {
         this.context = notNull(context, "context");
      }

      /** @implNote dynamic implementation to retrieve new parameters on backend/formatter update. */
      private Map<String, ParameterDescription<?>> parameterDescriptions() {
         return context
               .availableParameters()
               .stream()
               .collect(toMap(ParameterDescription::name, Function.identity()));
      }

      /**
       * @implSpec Manages the simple set command case and the aliased set command case.
       */
      @Override
      public void complete(LineReader reader, ParsedLine line, List<Candidate> candidates) {
         String option = null;
         if (line.wordIndex() > 0 && line.wordIndex() < 2) {
            Optional<ConsoleCommand> aliasTargetCommand = context
                  .commands()
                  .stream()
                  .filter(command -> command.commandName().equals(line.words().get(line.wordIndex() - 1)))
                  .findAny();
            if (aliasTargetCommand.isPresent() && aliasTargetCommand.get() instanceof AliasTargetCommand) {
               option = ((AliasTargetCommand) aliasTargetCommand.get()).aliasOptions().get(0);
            }
         } else if (line.wordIndex() > 0) {
            option = line.words().get(line.wordIndex() - 1);
         }

         if (option != null) {
            ParameterDescription<?> parameterDescription = parameterDescriptions().get(option);

            if (parameterDescription != null) {
               candidates.addAll(parameterDescription
                                       .completes(line.words().get(line.wordIndex()))
                                       .stream()
                                       .map(Candidate::new)
                                       .collect(toList()));
            }
         }

      }
   }
}
