/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.cli.converters;

import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE;
import static java.time.format.DateTimeFormatter.ISO_LOCAL_TIME;

import java.time.Clock;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.DateTimeParseException;
import java.time.temporal.TemporalAccessor;

import picocli.CommandLine.ITypeConverter;

public class FlexibleDateTimeConverter implements ITypeConverter<ZonedDateTime> {

   private final Clock clock;

   public FlexibleDateTimeConverter(Clock clock) {
      this.clock = clock;
   }

   public FlexibleDateTimeConverter(ZoneId timezone) {
      this.clock = timezone != null ? Clock.system(timezone) : Clock.systemDefaultZone();
   }

   public FlexibleDateTimeConverter() {
      this(Clock.systemDefaultZone());
   }

   @Override
   public ZonedDateTime convert(String value) {
      ZonedDateTime zdt = parseSpecialDates(value);
      if (zdt == null) zdt = parseDuration(value);
      if (zdt == null) zdt = parseDateTime(value);
      if (zdt == null) throw new IllegalArgumentException(String.format("Can't parse date '%s'", value));

      return zdt;
   }

   private ZonedDateTime parseSpecialDates(String value) {
      ZonedDateTime zdt = null;
      if ("now".equals(value)) {
         zdt =  now();
      }
      return zdt;
   }

   private ZonedDateTime parseDateTime(String value) {
      try {
         DateTimeFormatter formatter = new DateTimeFormatterBuilder()
               .parseCaseInsensitive()
               .append(ISO_LOCAL_DATE)
               .optionalStart()
               .appendLiteral('T')
               .append(ISO_LOCAL_TIME)
               .optionalStart()
               .appendOffsetId()
               .optionalStart()
               .appendLiteral('[')
               .parseCaseSensitive()
               .appendZoneRegionId()
               .appendLiteral(']')
               .optionalEnd()
               .optionalEnd()
               .optionalEnd()
               .toFormatter();

         TemporalAccessor temporalAccessor =
               formatter.parseBest(value, ZonedDateTime::from, LocalDateTime::from, LocalDate::from);
         if (temporalAccessor instanceof ZonedDateTime) {
            return ((ZonedDateTime) temporalAccessor);
         }
         if (temporalAccessor instanceof LocalDateTime) {
            return ((LocalDateTime) temporalAccessor).atZone(clock.getZone());
         }
         return ((LocalDate) temporalAccessor).atStartOfDay(clock.getZone());
      } catch (DateTimeParseException e) {
         return null;
      }
   }

   private ZonedDateTime parseDuration(String value) {
      try {
         return now().minus(Duration.parse(value));
      } catch (DateTimeParseException e) {
         return null;
      }
   }

   private ZonedDateTime now() {
      return ZonedDateTime.now(clock);
   }

}
